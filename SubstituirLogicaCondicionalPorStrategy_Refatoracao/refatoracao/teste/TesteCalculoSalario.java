package refatoracao.teste;
import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.pojo.Funcionario;
import refatoracao.pojo.Pessoa;
import refatoracao.pojo.Salario;
import refatoracao.pojo.TipoFuncionario;
import refatoracao.strategy.CalcularSalarioComissaoStrategyCoordenador;

public class TesteCalculoSalario {

	@Test
	public void test() {
		//Dados Pessoa
		Pessoa pessoa = new Pessoa();
		Calendar nascimento = Calendar.getInstance();
		nascimento.set(1980, 10, 15);
		
		pessoa.setDocumento("123456789");
		pessoa.setNascimento(nascimento);
		pessoa.setNome("Juliana Maria");
		
		//Dados Funcionario
		Calendar admissao = Calendar.getInstance();
		admissao.set(2010, 10, 15);
		Salario salario = new Salario();
		salario.setValorSalario(11000.00);
		
		Funcionario funcionario = 
				new Funcionario(pessoa,TipoFuncionario.COORDENADOR,
						salario,admissao, new CalcularSalarioComissaoStrategyCoordenador());
		
		
		Assert.assertTrue(funcionario.calcularSalarioComComissao().equals(new Double(13200.00)));
	}

}
