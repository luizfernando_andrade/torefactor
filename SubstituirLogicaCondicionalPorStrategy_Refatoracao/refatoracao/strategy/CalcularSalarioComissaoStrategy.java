package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public abstract class CalcularSalarioComissaoStrategy {
	
	abstract Double calcularComissao(Funcionario funcionario);
	
	public Double calcularSalarioComComissao(Funcionario funcionario){
		
		return funcionario.getSalario() + calcularComissao(funcionario); 
			
	}
}
