package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyComum extends CalcularSalarioComissaoStrategy {

	@Override
	Double calcularComissao(Funcionario funcionario) {
		return (funcionario.getSalario() * 0.1);
	}

}
