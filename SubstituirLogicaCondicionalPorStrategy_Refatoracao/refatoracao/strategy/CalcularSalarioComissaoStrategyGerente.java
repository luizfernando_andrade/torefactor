package refatoracao.strategy;

import refatoracao.pojo.Funcionario;

public class CalcularSalarioComissaoStrategyGerente extends CalcularSalarioComissaoStrategy {

	@Override
	Double calcularComissao(Funcionario funcionario) {
		return (funcionario.getSalario() * 0.3) 
				+ adicionalGerente(funcionario.tempoDeCasaEmAnos(), funcionario.getSalario());
	}

	
	private Double adicionalGerente(int tempoDeCasa,double salario){
		return ((tempoDeCasa * 5) / 100) * salario;
	}

}
