package refatoracao.pojo;

public enum TipoFuncionario {
	OPERACIONAL, 
	COORDENADOR, 
	GERENTE;
}
