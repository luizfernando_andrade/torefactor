package refatoracao.pojo;

import java.util.Calendar;

import refatoracao.pojo.Pessoa;
import refatoracao.pojo.Salario;
import refatoracao.pojo.TipoFuncionario;
import refatoracao.strategy.CalcularSalarioComissaoStrategy;

public class Funcionario {

	private CalcularSalarioComissaoStrategy calculoStrategy;
	private Pessoa pessoa;
	private TipoFuncionario tipoFuncionario;
	private Salario salario;
	private Calendar admissao;
	
	public Funcionario (Pessoa pessoa, TipoFuncionario tipoFuncionario, 
			Salario salario, Calendar admissao, CalcularSalarioComissaoStrategy calculoStrategy){
		this.pessoa = pessoa;
		this.tipoFuncionario = tipoFuncionario;
		this.salario = salario;
		this.calculoStrategy = calculoStrategy;
	}
	
	
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}
	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}
	public Double getSalario() {
		return salario.getValorSalario();
	}
	public void setSalario(Salario salario) {
		this.salario = salario;
	}
	public Calendar getAdmissao() {
		return admissao;
	}
	public void setAdmissao(Calendar admissao) {
		this.admissao = admissao;
	}
	
	public Integer tempoDeCasaEmAnos(){
		
		Integer anoAtual = Calendar.getInstance().get(Calendar.YEAR);
		Integer anoAdmissao = admissao.get(Calendar.YEAR);
		
		return anoAtual - anoAdmissao;
	}

	
	public Double calcularSalarioComComissao(){
	
		return calculoStrategy.calcularSalarioComComissao(this);
		
	}
	
	
	public CalcularSalarioComissaoStrategy getCalculoStrategy() {
		return calculoStrategy;
	}
	public void setCalculoStrategy(CalcularSalarioComissaoStrategy calculoStrategy) {
		this.calculoStrategy = calculoStrategy;
	}
	
}
