package maucheiro.bhaskara.pojo;

import java.util.HashMap;
import java.util.Map;

public class Expressao {

	HashMap<String, Double> eixoX = new HashMap<>();
	
	public Map<String, Double> calcularBhaskara(Double a, Double b, Double c){
		
		DeltaComposite delta = new DeltaComposite(a, b, c);
		BhaskaraX1 x1 = new BhaskaraX1(a, b, delta);
		BhaskaraX2 x2 = new BhaskaraX2(a, b, delta);
		eixoX.put("x1", x1.calcularBhaskaraX1());
		eixoX.put("x2", x2.calcularBhaskaraX2());
		
		return eixoX;
	}
	
}
