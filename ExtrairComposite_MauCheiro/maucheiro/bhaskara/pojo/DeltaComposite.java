package maucheiro.bhaskara.pojo;

public class DeltaComposite {
	
	private Double a;
	private Double b;
	private Double c;

	public DeltaComposite(Double a, Double b, Double c){
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public double calcularDelta() {
		return Math.pow(b, 2) - (4*a*c);
	}
}