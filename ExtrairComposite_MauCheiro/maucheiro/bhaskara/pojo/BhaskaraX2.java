package maucheiro.bhaskara.pojo;

public class BhaskaraX2 {
	
	private DeltaComposite delta;
	private Double a;
	private Double b;

	public BhaskaraX2(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}

	public double calcularBhaskaraX2() {
		return (-b - Math.sqrt( delta.calcularDelta()))/2*a;
	}
}
