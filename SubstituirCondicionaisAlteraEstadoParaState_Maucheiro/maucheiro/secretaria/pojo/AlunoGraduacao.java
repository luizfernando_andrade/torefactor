package maucheiro.secretaria.pojo;

import java.util.ArrayList;
import java.util.List;

public class AlunoGraduacao {

	private List<Double> notas = new ArrayList<Double>();
	private Integer faltas;
	private String nome;
	private String matricula;
	private EstadoAluno estadoAluno;
	
	public AlunoGraduacao(String nome, String matricula) {
		this.nome = nome;
		this.matricula = matricula;
	}
	
	public List<Double> getNotas() {
		return notas;
	}
	public void setNotas(List<Double> notas) {
		this.notas = notas;
	}
	public Integer getFaltas() {
		return faltas;
	}
	public void setFaltas(Integer faltas) {
		this.faltas = faltas;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getMatricula() {
		return matricula;
	}
	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public Double media(){
		Double somatorio = 0.0;
		for (Double nota : notas) {
			somatorio = somatorio + nota;
		}
		return somatorio/ notas.size(); 
	}
	
	public void calcularAprovacaoAluno(){
		if (faltas < 19){
			if (media() >= 6.0 ){
				estadoAluno = EstadoAluno.Aprovado;
			}
			if (media() >= 5.0 && media() < 6.0 ){
				estadoAluno = EstadoAluno.ProvaFinal;
			}
			if (media() < 5.0){
				estadoAluno =  EstadoAluno.Reprovado;
			}
		} else {
			estadoAluno =  EstadoAluno.Reprovado;
		}
	}
	
	public String acaoAluno(){
		String acao;
		if (EstadoAluno.Aprovado.equals(estadoAluno)){
			acao = "Encerrar disciplina";
		}else if (EstadoAluno.ProvaFinal.equals(estadoAluno)){
			acao = "Marcar Prova Final";
		} else {
			acao = "Rematricula na disciplina";
		}
		return acao;
		
	}

	public EstadoAluno getEstadoAluno() {
		return estadoAluno;
	}

	public void setEstadoAluno(EstadoAluno estadoAluno) {
		this.estadoAluno = estadoAluno;
	}
	
}
