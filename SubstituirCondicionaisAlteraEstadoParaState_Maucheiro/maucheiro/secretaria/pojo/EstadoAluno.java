package maucheiro.secretaria.pojo;

public enum EstadoAluno {
	Aprovado, Reprovado, ProvaFinal;
}
