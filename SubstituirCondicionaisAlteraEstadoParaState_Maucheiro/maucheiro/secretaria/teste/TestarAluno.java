package maucheiro.secretaria.teste;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.secretaria.pojo.AlunoGraduacao;
import maucheiro.secretaria.pojo.EstadoAluno;

public class TestarAluno {

	@Test
	public void testarAlunoAprovado(){
		AlunoGraduacao alunoGraduacao = new AlunoGraduacao("Marcelo Moreno", "123456");
		List<Double> notas = new ArrayList<Double>();
		
		notas.add(7.0);
		notas.add(9.0);
		notas.add(8.0);
		notas.add(9.0);
		
		alunoGraduacao.setFaltas(10);		
		alunoGraduacao.setNotas(notas);
		alunoGraduacao.calcularAprovacaoAluno();
		
		Assert.assertTrue(EstadoAluno.Aprovado.equals(alunoGraduacao.getEstadoAluno()));
	}
}
