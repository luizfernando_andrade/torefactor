package refatoracao.conector.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.conector.pojo.Conector;
import refatoracao.conector.pojo.Conexao;
import refatoracao.conector.pojo.ConfiguraConexao;
import refatoracao.conector.pojo.TipoBaseDados;
import refatoracao.conector.pojo.TipoConector;

public class TesteConexaoBD {

	@Test
	public void TestarConexaoCertificacao(){
		ConfiguraConexao configuraConexao = new ConfiguraConexao();
		
		configuraConexao.setUsuario("root");
		configuraConexao.setSenha("12345678909");
		configuraConexao.setBaseDados(TipoBaseDados.CERTIFICACAO);
		
		
		Conector conector = new Conector();
		
		conector.setConfiguraConexao(configuraConexao);
		
		conector.setTipoConector(TipoConector.MSQL);
		
		Conexao conexao = conector.criarConexao();
		
		Assert.assertTrue(conexao.getUrlConexao().contains("cert"));
	}
}
