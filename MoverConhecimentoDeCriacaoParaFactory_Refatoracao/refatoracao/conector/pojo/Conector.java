package refatoracao.conector.pojo;

public class Conector extends ConexaoFactory {

	TipoConector tipoConector;
	
	TipoConexao tipoConexao;

	public TipoConector getTipoConector() {
		return tipoConector;
	}

	public void setTipoConector(TipoConector tipoConector) {
		this.tipoConector = tipoConector;
	}
	
	public String getInfoConexao() {
		
		return "Conectado a: " + getConfiguraConexao().getBaseDados().name() 
				+ " com tipo de acesso  " + tipoConexao.tipoAcesso();
	}

	@Override
	void adicionarTipoConexao(TipoConexao tipoConexao) {
		this.tipoConexao = tipoConexao;
		
	}

}
