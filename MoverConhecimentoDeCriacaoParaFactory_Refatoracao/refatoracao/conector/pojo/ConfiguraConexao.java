package refatoracao.conector.pojo;

public class ConfiguraConexao {

	private String usuario;
	private String senha;
	private TipoBaseDados baseDados;

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public void setBaseDados(TipoBaseDados baseDados) {
		this.baseDados = baseDados;
	}

	public String getSenha() {
		return senha;
	}

	public TipoBaseDados getBaseDados() {
		return baseDados;
	}

}
