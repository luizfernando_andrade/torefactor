package refatoracao.conector.pojo;

public class Cliente {

	public static void main(String[] args) {
		
		ConfiguraConexao configuraConexao = new ConfiguraConexao();
		
		configuraConexao.setUsuario("root");
		configuraConexao.setSenha("12345678909");
		configuraConexao.setBaseDados(TipoBaseDados.CERTIFICACAO);
		
		
		Conector conector = new Conector();
		
		conector.setConfiguraConexao(configuraConexao);
		
		conector.setTipoConector(TipoConector.MSQL);
		
		Conexao conexao = conector.criarConexao();
		
		System.out.println(conexao.getUrlConexao());
	}
}
