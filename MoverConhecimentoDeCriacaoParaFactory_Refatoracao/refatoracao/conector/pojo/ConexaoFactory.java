package refatoracao.conector.pojo;

public abstract class ConexaoFactory {
	
	ConfiguraConexao configuraConexao; 
	
	abstract void adicionarTipoConexao(TipoConexao tipoConexao);
	
	public Conexao criarConexao(){
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		adicionarTipoConexao(tipoConexao);
		
		if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.CERTIFICACAO)){
			return new Conexao("Conectado a: certificacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.HOMOLOGACAO)){
			return new Conexao("Conectado a: homologacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.PRODUCAO)){
			return new Conexao("Conectado a: producao/aplicacao");
		} else {
			return new Conexao("Conectado a: localhost/aplicacao");
		}
	}

	public ConfiguraConexao getConfiguraConexao() {
		return configuraConexao;
	}

	public void setConfiguraConexao(ConfiguraConexao configuraConexao) {
		this.configuraConexao = configuraConexao;
	}
	
}
