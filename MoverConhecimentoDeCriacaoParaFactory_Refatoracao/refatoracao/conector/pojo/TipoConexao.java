package refatoracao.conector.pojo;

public class TipoConexao {

	private ConfiguraConexao configuraConexao;

	public TipoConexao(ConfiguraConexao configuraConexao) {
		this.configuraConexao = configuraConexao;
	}

	public ConfiguraConexao getConfiguraConexao() {
		return configuraConexao;
	}

	public void setConfiguraConexao(ConfiguraConexao configuraConexao) {
		this.configuraConexao = configuraConexao;
	}
	
	public String tipoAcesso(){
		if (!configuraConexao.getBaseDados().equals(TipoBaseDados.DESENVOLVIMENTO)){
			return "Somente leitura";
		} else {
			return "Acesso de total";
		}
	}

	
}
