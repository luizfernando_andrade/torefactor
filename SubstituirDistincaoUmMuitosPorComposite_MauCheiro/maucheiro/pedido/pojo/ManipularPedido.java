package maucheiro.pedido.pojo;

import java.util.ArrayList;
import java.util.List;

public class ManipularPedido {

	public List<Pedido> liberarPedido(List<Pedido> pedidos){
		for (Pedido pedido : pedidos) {
			pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		}
		return pedidos;
	}
	
	public List<Pedido> liberarPedido(Pedido pedido) {
		
		List<Pedido> pedidos = new ArrayList<Pedido>();
		pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		pedidos.add(pedido);
		return pedidos;
	}
}
