package maucheiro.pedido.pojo;

import java.util.Calendar;
import java.util.List;

public class Pedido {

	private SituacaoPedido situacaoPedido;
	private List<Item> itens;
	private Calendar dataPedido;
	
	public Pedido(SituacaoPedido situacaoPedido) {
		this.situacaoPedido = situacaoPedido;
	}
	
	public SituacaoPedido getSituacaoPedido() {
		return situacaoPedido;
	}
	public void setSituacaoPedido(SituacaoPedido situacaoPedido) {
		this.situacaoPedido = situacaoPedido;
	}
	public List<Item> getItens() {
		return itens;
	}
	public void setItens(List<Item> itens) {
		this.itens = itens;
	}
	public Calendar getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(Calendar dataPedido) {
		this.dataPedido = dataPedido;
	}
	
}
