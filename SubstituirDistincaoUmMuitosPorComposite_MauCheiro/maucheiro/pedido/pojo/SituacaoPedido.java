package maucheiro.pedido.pojo;

public enum SituacaoPedido {
	Pendente, Cancelado, Liberado;
}
