package maucheiro.pedido.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.pedido.pojo.Item;
import maucheiro.pedido.pojo.ManipularPedido;
import maucheiro.pedido.pojo.Pedido;
import maucheiro.pedido.pojo.SituacaoPedido;

public class TestarLiberacaoPedido {

	@Test
	public void liberarPedidoUnico(){
		boolean pedidoPendente = false;
		
		Pedido pedido = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		
		List<Item> itens = new ArrayList<Item>();
		itens.add(new Item("protetor auricular"));
		
		pedido.setItens(itens);
		ManipularPedido manipularPedido = new ManipularPedido();
		
		for (Pedido unidade : manipularPedido.liberarPedido(pedido)) {
			if (unidade.getSituacaoPedido().equals(SituacaoPedido.Pendente)){
				pedidoPendente = true;
			}
		} 
		
		Assert.assertTrue(!pedidoPendente);
	}
	
	@Test
	public void liberarPedidos(){
		boolean pedidoPendente = false;
		List<Item> itens = new ArrayList<Item>();
		itens.add(new Item("protetor auricular"));
		itens.add(new Item("protetor bucal"));
		itens.add(new Item("protetor nasal"));
		
		List<Pedido> pedidos = new ArrayList<>();
		
		Pedido pedido = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido);
		Pedido pedido2 = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido2);
		Pedido pedido3 = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido3);
		
		
		ManipularPedido manipularPedido = new ManipularPedido();
		
		for (Pedido unidade : manipularPedido.liberarPedido(pedidos)) {
			if (unidade.getSituacaoPedido().equals(SituacaoPedido.Pendente)){
				pedidoPendente = true;
			}
		} 
		Assert.assertTrue(!pedidoPendente);
	}
	
}
