package maucheiro.equacao.pojo;

import java.util.HashMap;
import java.util.Map;

public class Expressao {

	HashMap<String, Double> eixoX = new HashMap<>();
	
	public Map<String, Double> calcularBhaskara(Double a, Double b, Double c){
		
		eixoX.put("x1", ((-b + Math.sqrt( Math.pow(b, 2) - (4*a*c)))/2*a));
		eixoX.put("x2", ((-b - Math.sqrt( Math.pow(b, 2) - (4*a*c)))/2*a));
		
		return eixoX;
	}
	
}
