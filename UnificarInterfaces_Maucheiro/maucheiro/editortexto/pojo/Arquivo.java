package maucheiro.editortexto.pojo;

public interface Arquivo {

	String corpo();
	String cabecalho();
	String rocape();
}
