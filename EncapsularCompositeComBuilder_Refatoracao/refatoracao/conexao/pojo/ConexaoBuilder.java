package refatoracao.conexao.pojo;

public class ConexaoBuilder {

	public Conexao montarConexao(String usuario, 
			String senha, TipoBaseDados baseDados){
		
		ConfiguraConexao configuraConexao = 
				new ConfiguraConexao(usuario,senha,baseDados);
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		Conector conector = new Conector(tipoConexao);
		
		Conexao conexao = conector.criarConexao();
		return conexao;
	}
}
