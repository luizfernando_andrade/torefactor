package refatoracao.conexao.pojo;

public enum TipoBaseDados {

	CERTIFICACAO, HOMOLOGACAO, PRODUCAO, DESENVOLVIMENTO;
}
