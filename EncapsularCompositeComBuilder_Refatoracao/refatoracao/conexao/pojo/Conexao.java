package refatoracao.conexao.pojo;

public class Conexao {

	private String urlConexao;

	public Conexao(String urlConexao) {
		this.urlConexao = urlConexao;
	}

	public String getUrlConexao() {
		return urlConexao;
	}

	public void setUrlConexao(String urlConexao) {
		this.urlConexao = urlConexao;
	}

}
