package refatoracao.conexao.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.conexao.pojo.Conexao;
import refatoracao.conexao.pojo.ConexaoBuilder;
import refatoracao.conexao.pojo.TipoBaseDados;

public class TestConexao {
	
	@Test
	public void testarConexao(){
		ConexaoBuilder builder = new ConexaoBuilder();
		
		Conexao conexao = builder.montarConexao("admin", "abcd01", TipoBaseDados.PRODUCAO);
		
		Assert.assertTrue(conexao != null);
	}
}
