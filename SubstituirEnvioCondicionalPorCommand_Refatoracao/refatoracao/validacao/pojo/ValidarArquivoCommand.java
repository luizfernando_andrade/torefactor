package refatoracao.validacao.pojo;

public interface ValidarArquivoCommand {

	public String executar(ArquivoDeIntergracao arquivo);
}
