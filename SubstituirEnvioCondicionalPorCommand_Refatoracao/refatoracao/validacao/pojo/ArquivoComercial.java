package refatoracao.validacao.pojo;

public class ArquivoComercial {

	Double valorTotalComercial;

	public Double getValorTotalComercial() {
		return valorTotalComercial;
	}

	public void adicionarValorTotalComercial(Double valor) {
		this.valorTotalComercial = valor;
	}
	
	public String setorResponsavel(){
		return "Comercial";
	}
}
