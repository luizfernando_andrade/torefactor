package refatoracao.validacao.pojo;

public class ValidarArquivoFinanceiro implements ValidarArquivoCommand {

	@Override
	public String executar(ArquivoDeIntergracao arquivo) {
		ArquivoFinanceiro financeiro = new ArquivoFinanceiro();
		financeiro.adicionarValorTotalFinanceiro(arquivo.getImpostosPagos());
		arquivo.setResponsavel(financeiro.setorResponsavel());
		return arquivo.getResponsavel();
	}

}
