package refatoracao.validacao.pojo;

public class ArquivoDeIntergracao {

	private TipoArquivo tipoArquivo;
	private String responsavel;
	private Double vendasMes;
	private Double volumeGasto;
	private Double impostosPagos;
	
	
	public TipoArquivo getTipoArquivo() {
		return tipoArquivo;
	}
	public void setTipoArquivo(TipoArquivo tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}
	public String getResponsavel() {
		return responsavel;
	}
	public void setResponsavel(String responsavel) {
		this.responsavel = responsavel;
	}
	
	//Valores do setor financeiro
	public Double getVendasMes() {
		return vendasMes;
	}
	public void setVendasMes(Double vendasMes) {
		this.vendasMes = vendasMes;
	}
	
	//Valores do setor estoque
	public Double getVolumeGasto() {
		return volumeGasto;
	}
	public void setVolumeGasto(Double volumeGasto) {
		this.volumeGasto = volumeGasto;
	}
	
	//Valores do setor Financeiro
	public Double getImpostosPagos() {
		return impostosPagos;
	}
	public void setImpostosPagos(Double impostosPagos) {
		this.impostosPagos = impostosPagos;
	}
	
}
