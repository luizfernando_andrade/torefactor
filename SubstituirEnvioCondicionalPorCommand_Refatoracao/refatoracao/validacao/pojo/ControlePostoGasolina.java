package refatoracao.validacao.pojo;

public class ControlePostoGasolina {

	ArquivoDeIntergracao arquivo;
	ValidarArquivoCommand validarArquivo;
	
	public ControlePostoGasolina(ArquivoDeIntergracao arquivo,ValidarArquivoCommand validarArquivo) {
		this.arquivo = arquivo;
		this.validarArquivo = validarArquivo;
	}
	
	public String validarArquivo(){
		return validarArquivo.executar(arquivo);
	}
}
