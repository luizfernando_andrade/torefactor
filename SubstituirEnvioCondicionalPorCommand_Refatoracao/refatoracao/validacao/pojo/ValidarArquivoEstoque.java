package refatoracao.validacao.pojo;

public class ValidarArquivoEstoque implements ValidarArquivoCommand {

	@Override
	public String executar(ArquivoDeIntergracao arquivo) {
		ArquivoEstoque estoque= new ArquivoEstoque();
		estoque.adicionarValor(arquivo.getImpostosPagos());
		arquivo.setResponsavel(estoque.setorResponsavel());
		return arquivo.getResponsavel();
	}

}
