package refatoracao.validacao.pojo;

public class ValidarArquivoComercial implements ValidarArquivoCommand {

	@Override
	public String executar(ArquivoDeIntergracao arquivo) {
		ArquivoComercial comercial = new ArquivoComercial();
		comercial.adicionarValorTotalComercial(arquivo.getImpostosPagos());
		arquivo.setResponsavel(comercial.setorResponsavel());
		return arquivo.getResponsavel();
	}

}
