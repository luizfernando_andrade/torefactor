package refatoracao.validacao.pojo;

public class ArquivoEstoque {

	Double valor;

	public Double getValor() {
		return valor;
	}

	public void adicionarValor(Double valor) {
		this.valor = valor;
	}
	
	public String setorResponsavel(){
		return "Estoque";
	}

}
