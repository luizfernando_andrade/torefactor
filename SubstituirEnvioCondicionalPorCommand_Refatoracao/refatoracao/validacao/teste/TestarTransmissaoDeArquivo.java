package refatoracao.validacao.teste;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.validacao.pojo.ArquivoDeIntergracao;
import refatoracao.validacao.pojo.ControlePostoGasolina;
import refatoracao.validacao.pojo.TipoArquivo;
import refatoracao.validacao.pojo.ValidarArquivoFinanceiro;

public class TestarTransmissaoDeArquivo {

	@Test
	public void testarTransmissaoFinanceiro(){
		ArquivoDeIntergracao arquivo = new ArquivoDeIntergracao();
		arquivo.setImpostosPagos(35.0);
		arquivo.setResponsavel("Gerente Financeiro");
		arquivo.setTipoArquivo(TipoArquivo.Financeiro);
		arquivo.setImpostosPagos(200.0);
		
		ControlePostoGasolina posto = new ControlePostoGasolina(arquivo, new ValidarArquivoFinanceiro());
		String validadoPor = posto.validarArquivo();
		System.out.println(validadoPor);
		
		Assert.assertTrue(validadoPor.contains("Financeiro"));
		
	}
}
