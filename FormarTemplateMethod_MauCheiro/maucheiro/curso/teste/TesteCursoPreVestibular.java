package maucheiro.curso.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.curso.pojo.Curso;
import maucheiro.curso.pojo.CursoExtensivo;
import maucheiro.curso.pojo.CursoIntensivo;

public class TesteCursoPreVestibular {

	Curso curso = null;
	
	@Test
	public void testarCursoIntensivo(){
		curso = new CursoIntensivo();
		String descricaoCurso = 
		curso.infoCurso()
		.concat(curso.prepararAula())
		.concat(curso.avaliacaoSemanal());
		
		Assert.assertTrue(descricaoCurso.
				contains("Intensivo"));
	}
	
	@Test
	public void testarCursoExtensivo(){
		curso = new CursoExtensivo();
		String descricaoCurso = 
		curso.infoCurso()
		.concat(curso.prepararAula())
		.concat(curso.avaliacaoSemanal());
		
		Assert.assertTrue(descricaoCurso.
				contains("Extensivo"));
	}
}
