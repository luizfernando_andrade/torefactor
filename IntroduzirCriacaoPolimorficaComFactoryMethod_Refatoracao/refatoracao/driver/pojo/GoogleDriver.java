package refatoracao.driver.pojo;

public class GoogleDriver extends WebDriver {

	//...
	
	@Override
	protected String recuperarBrowser() {
		
		return "Google Chrome";
	}

	@Override
	public String abrirBrowser() {
		return "Abrindo browser " + recuperarBrowser();
	}

	
}
