package refatoracao.driver.teste;

import refatoracao.driver.pojo.GoogleDriver;
import refatoracao.driver.pojo.WebDriver;

public class TestDriverChrome extends TestDriver {

	protected WebDriver newDriver() {
		WebDriver driver = new GoogleDriver();
		return driver;
	}

}
