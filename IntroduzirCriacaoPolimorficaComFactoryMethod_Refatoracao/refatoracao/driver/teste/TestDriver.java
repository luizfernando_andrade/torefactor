package refatoracao.driver.teste;

import refatoracao.driver.pojo.WebDriver;

public abstract class TestDriver {

	protected abstract WebDriver newDriver();
	
	public String direcionarURL(String url) {
		
		WebDriver driver = newDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}
}
