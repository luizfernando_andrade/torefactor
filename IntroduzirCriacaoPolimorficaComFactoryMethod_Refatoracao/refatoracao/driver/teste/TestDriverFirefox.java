package refatoracao.driver.teste;

import refatoracao.driver.pojo.FirefoxDriver;
import refatoracao.driver.pojo.WebDriver;

public class TestDriverFirefox extends TestDriver{

	protected WebDriver newDriver() {
		WebDriver driver = new FirefoxDriver();
		return driver;
	}

}
