package refatoracao.driver.teste;

import org.junit.Test;

import junit.framework.Assert;

public class TestarWebDriver {

	@Test
	public void testarDriverChrome(){
		
		TestDriverChrome driver = new TestDriverChrome();
		
		String browser = driver.direcionarURL("http://www.google.com");
		
		Assert.assertTrue(browser.contains("Chrome"));
	}
	@Test
	public void testarDriverFirefox(){
		
		TestDriverFirefox driver = new TestDriverFirefox();
		
		String browser = driver.direcionarURL("http://www.google.com");
		
		Assert.assertTrue(browser.contains("Firefox"));
	}
}
