package refatoracao.editortexto.pojo;

public interface Arquivo {

	String corpo();
	String cabecalho();
	String rodape();
}
