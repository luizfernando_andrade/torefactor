package refatoracao.editortexto.pojo;

public class ArquivoWord implements Arquivo {

	@Override
	public String corpo() {
		return "Corpo do arquivo";
	}

	@Override
	public String cabecalho() {
		return "Cabe�alho";
	}

	@Override
	public String rodape() {
		return "Rodap�";
	}
	
	public String encoding(){
		return "UTF-8";
	}

}
