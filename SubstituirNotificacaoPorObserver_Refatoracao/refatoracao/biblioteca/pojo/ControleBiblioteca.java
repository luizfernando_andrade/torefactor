package refatoracao.biblioteca.pojo;

public class ControleBiblioteca implements ObservarLivro {

	private String log;
	
//	public void devolucao(Livro livro){
//		log = livro.devolverLivro();
//	}
	
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}

	@Override
	public String notificar(Livro livro) {
		if (EstadoLivro.reservado.equals(livro.getEstadoLivro())){
			log = "Livro possui reserva";
			System.out.println(log);
		} else {
			log = "Livro liberado"; 
		}
		System.out.println(log);
		return log;
	}
}
