package refatoracao.biblioteca.pojo;

public interface ObservarLivro {
	
public String notificar(Livro livro);

}