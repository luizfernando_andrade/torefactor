package refatoracao.biblioteca.pojo;

public enum EstadoLivro {

	liberado, reservado, emprestado;
}
