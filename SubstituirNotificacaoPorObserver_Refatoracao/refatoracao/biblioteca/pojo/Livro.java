package refatoracao.biblioteca.pojo;

import java.util.ArrayList;
import java.util.List;

public class Livro {

	private String titulo;
	private EstadoLivro estadoLivro;
	private NotificarReserva notificacao = new NotificarReserva();
	List<ObservarLivro> observers = new ArrayList<>();
	
	public void adicionarObservador(ObservarLivro observador){
		observers.add(observador);
	}
	
	public void removerObservador(ObservarLivro observador){
		observers.remove(observador);
	}
	
	public void notificarObservadores(){
		for (ObservarLivro observarLivro : observers) {
			observarLivro.notificar(this);
		}
	}
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public EstadoLivro getEstadoLivro() {
		return estadoLivro;
	}
	public void setEstadoLivro(EstadoLivro estadoLivro) {
		this.estadoLivro = estadoLivro;
	}
	public NotificarReserva getNotificacao() {
		return notificacao;
	}
	public void setNotificacao(NotificarReserva notificacao) {
		this.notificacao = notificacao;
	}
	
	public void devolverLivro(){
		notificarObservadores();
		setEstadoLivro(EstadoLivro.liberado);
	}
}
