package refatoracao.biblioteca.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.biblioteca.pojo.ControleBiblioteca;
import refatoracao.biblioteca.pojo.EstadoLivro;
import refatoracao.biblioteca.pojo.Livro;

public class TesteEmprestimo {

	@Test
	public void testarEmprestimo(){
		//preparar teste
		Livro livro = new Livro();
		livro.setEstadoLivro(EstadoLivro.reservado);
		livro.setTitulo("LivroTeste");
		
		livro.adicionarObservador(new ControleBiblioteca());
		
		//testar devolu��o
		livro.devolverLivro();
		
		Assert.assertTrue(EstadoLivro.liberado.equals(livro.getEstadoLivro()));
	}
}
