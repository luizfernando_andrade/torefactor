package maucheiro.calculo.teste;

import org.junit.Test;

import junit.framework.Assert;
import maucheiro.calculo.pojo.EquacaoSegundoGrau;

public class TestarEquacaoSegundoGrau {

	@Test
	public void realizarCalculoDelta(){
		
		EquacaoSegundoGrau equacao = new EquacaoSegundoGrau();
		equacao.setA(3);
		equacao.setB(-7);
		equacao.setC(2);
		
		Double delta = equacao.valorDelta();
		
		Assert.assertTrue(delta == 25.0);
		
	}
}
