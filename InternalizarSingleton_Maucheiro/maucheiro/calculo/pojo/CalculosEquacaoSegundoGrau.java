package maucheiro.calculo.pojo;

public class CalculosEquacaoSegundoGrau {

	private static CalculosEquacaoSegundoGrau instancia = new CalculosEquacaoSegundoGrau();
	
	private CalculosEquacaoSegundoGrau(){
		
	}
	public static CalculosEquacaoSegundoGrau getInstancia() {
		if (instancia == null){
			instancia = new CalculosEquacaoSegundoGrau();
		}
		return instancia;
	}
	public Double calcularDelta(double a, double b, double c){
		return (b*b)- 4*a*c;
	}
	public Double calcularX1(double a, double b, double c, double delta){
		return (-b+Math.sqrt(delta))/2*a;
	}
	public Double calcularX2(double a, double b, double c, double delta){
		return (-b-Math.sqrt(delta))/2*a;
	}
}
