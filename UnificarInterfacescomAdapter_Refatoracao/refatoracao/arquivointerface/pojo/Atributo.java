package refatoracao.arquivointerface.pojo;

import java.util.ArrayList;
import java.util.List;

public class Atributo implements CampoArquivo {
	private Integer atributosLinha;
	private List<String> conteudo = new ArrayList<>();
	
	public Integer getAtributosLinha() {
		return atributosLinha;
	}
	public void setAtributosLinha(Integer atributosLinha) {
		this.atributosLinha = atributosLinha;
	}

	@Override
	public List<String> getConteudo() {
		return conteudo;
	}

	@Override
	public void adicionarConteudo(String conteudo) {
		this.conteudo.add(conteudo);
	}
}
