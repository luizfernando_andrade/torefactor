package refatoracao.arquivointerface.pojo;

import java.util.List;

public class CelulaAdapter implements CampoArquivo{

	private Celula celula = new Celula();
	
	@Override
	public List<String> getConteudo() {
		return celula.getValores();
	}

	@Override
	public void adicionarConteudo(String conteudo) {
		celula.setValores(conteudo);
	}
}
