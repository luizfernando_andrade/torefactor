package refatoracao.arquivointerface.pojo;

public abstract class ArquivoInterfaceBuilder {

	public abstract String montarArquivo();
}
