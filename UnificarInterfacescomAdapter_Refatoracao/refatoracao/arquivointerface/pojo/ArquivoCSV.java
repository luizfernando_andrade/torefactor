package refatoracao.arquivointerface.pojo;

public class ArquivoCSV extends ArquivoInterfaceBuilder{
	
	Atributo atributo = new Atributo();
	
	@Override
	public String montarArquivo() {
		String arquivo = "";
		int coluna = 0;
		for(int i = 0; i < atributo.getConteudo().size(); i++){
			if (coluna == atributo.getAtributosLinha()){
				arquivo.concat("\n");
			} else {
				arquivo.concat("|"+atributo.getConteudo().get(i)+"|");
			}
		}
		return arquivo;
	}

	public Atributo getAtributo() {
		return atributo;
	}

	public void addAtributo(String atributo) {
		this.atributo.adicionarConteudo(atributo);
	}

}
