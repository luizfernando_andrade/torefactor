package refatoracao.arquivointerface.pojo;

import java.util.List;

public interface CampoArquivo {

	List<String> getConteudo();

	void adicionarConteudo(String conteudo);

}