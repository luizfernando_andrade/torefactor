package refatoracao.calculo.pojo;

public class EquacaoSegundoGrau {

	private double a;
	private double b;
	private double c;
	private double delta;
	
	public double getA() {
		return a;
	}
	public void setA(double a) {
		this.a = a;
	}
	public double getB() {
		return b;
	}
	public void setB(double b) {
		this.b = b;
	}
	public double getC() {
		return c;
	}
	public void setC(double c) {
		this.c = c;
	}
	
	public Double calcularDelta(){
		delta = (b*b)- 4*a*c; 
		return delta;
	}
	public Double calcularX1(){
		return (-b+Math.sqrt(delta))/2*a;
	}
	public Double calcularX2(){
		return (-b-Math.sqrt(delta))/2*a;
	}
	
}
