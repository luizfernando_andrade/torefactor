package refatoracao.calculo.pojo;

public class CalculosEquacaoSegundoGrau {

	private static CalculosEquacaoSegundoGrau instancia = new CalculosEquacaoSegundoGrau();
	
	private CalculosEquacaoSegundoGrau(){
		
	}
	public static CalculosEquacaoSegundoGrau getInstancia() {
		if (instancia == null){
			instancia = new CalculosEquacaoSegundoGrau();
		}
		return instancia;
	}

}
