package refatoracao.calculo.teste;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.calculo.pojo.EquacaoSegundoGrau;

public class TestarEquacaoSegundoGrau {

	@Test
	public void realizarCalculoDelta(){
		
		EquacaoSegundoGrau equacao = new EquacaoSegundoGrau();
		equacao.setA(3);
		equacao.setB(-7);
		equacao.setC(2);
		
		Double delta = equacao.calcularDelta();
		
		Assert.assertTrue(delta == 25.0);
		
	}
}
