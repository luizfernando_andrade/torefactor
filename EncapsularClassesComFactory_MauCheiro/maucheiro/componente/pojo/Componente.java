package maucheiro.componente.pojo;

public abstract class Componente {

	private String nome;
	
	protected abstract String tipoComponente();

	public Componente(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
}
