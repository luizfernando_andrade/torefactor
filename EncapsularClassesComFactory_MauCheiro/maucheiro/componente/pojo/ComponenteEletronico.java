package maucheiro.componente.pojo;

public class ComponenteEletronico extends Componente {

	public ComponenteEletronico(String nome) {
		super(nome);
	}
	@Override
	protected String tipoComponente() {
		return "Componente Eletrônico";
	}

}
