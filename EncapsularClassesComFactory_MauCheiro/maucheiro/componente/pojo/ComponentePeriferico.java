package maucheiro.componente.pojo;

public class ComponentePeriferico extends Componente {

	public ComponentePeriferico(String nome) {
		super(nome);
	}
	@Override
	protected String tipoComponente() {
		return "Componente Periférico";
	}

}
