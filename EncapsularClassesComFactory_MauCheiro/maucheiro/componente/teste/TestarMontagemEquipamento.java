package maucheiro.componente.teste;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.componente.pojo.Componente;
import maucheiro.componente.pojo.ComponenteEletronico;
import maucheiro.componente.pojo.ComponenteMecanico;
import maucheiro.componente.pojo.ComponentePeriferico;
import maucheiro.componente.pojo.MontarEquipamento;

public class TestarMontagemEquipamento {

	@Test
	public void testarMontagem(){
		MontarEquipamento equipamento = new MontarEquipamento();
		List<Componente> componentes = new ArrayList<>();
		
		componentes.add(new ComponenteEletronico("fuz�vel"));
		componentes.add(new ComponenteMecanico("Alavanca disjuntor"));
		componentes.add(new ComponentePeriferico("Cabo de alta tens�o"));
		
		equipamento.setDescricao("Painel el�trico");
		equipamento.setComponentes(componentes);
		
		Assert.assertTrue(equipamento.getComponentes() != null
						  && equipamento.getComponentes().size() == 3);
	}
}
