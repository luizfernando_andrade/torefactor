package refatoracao.tiporelatorio.teste;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.tipoarquivo.pojo.TipoArquivoPDF;
import refatoracao.tiporelatorio.pojo.TipoRelatorio;
import refatoracao.tiporelatorio.pojo.TipoRelatorioFinanceiro;

public class TestarRelatorio {

	@Test
	public void TestarRelatorioFinanceiroPDF(){
		TipoRelatorio relatorio = new TipoRelatorioFinanceiro();
		
		String nomeRelatorio = relatorio.imprimirRelatorio(new TipoArquivoPDF());
		
		Assert.assertTrue(nomeRelatorio.contains("Financeiro")
				&& nomeRelatorio.contains("pdf"));
	}
	
}
