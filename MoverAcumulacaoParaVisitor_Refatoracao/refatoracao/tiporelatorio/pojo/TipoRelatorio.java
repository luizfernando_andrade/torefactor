package refatoracao.tiporelatorio.pojo;

import refatoracao.tipoarquivo.pojo.TipoArquivo;

public abstract class TipoRelatorio {
	
	public abstract String tipoRelatorio();
	
	public String imprimirRelatorio(TipoArquivo tipoArquivo){
		
		return tipoRelatorio().concat(tipoArquivo.extensaoArquivo());
		
	}
}
