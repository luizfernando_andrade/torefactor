package refatoracao.tipoarquivo.pojo;

public class TipoArquivoDoc extends TipoArquivo {

	@Override
	public String extensaoArquivo() {

		return ".doc";
	}

}
