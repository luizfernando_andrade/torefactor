package refatoracao.tipoarquivo.pojo;

public class TipoArquivoPDF extends TipoArquivo {

	@Override
	public String extensaoArquivo() {
		
		return ".pdf";
	}

}
