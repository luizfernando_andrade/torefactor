package refatoracao.sistema.pojo;

public class AccessIPV4 extends AccessPoint {

	IPV4 ipv4 = new IPV4();
	
	public void criarIP(String ip, String mask){
		ipv4.setEndereco(ip);
		ipv4.setMascara(mask);
	}
	public String direcionarIP(){
		return ipv4.direcionarIP(ipv4);
	}
}
