package refatoracao.sistema.pojo;

public class IPV4 {

	private String endereco;
	private String mascara;
	
	public String direcionarIP(IPV4 ipv4){
		return "Redirect to IPV4: " + ipv4.getEndereco();
	}
	
	public String enviarPacoteViaIP(){
		return "Enviando via IPV4";
	}
	
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getMascara() {
		return mascara;
	}
	public void setMascara(String mascara) {
		this.mascara = mascara;
	}
}
