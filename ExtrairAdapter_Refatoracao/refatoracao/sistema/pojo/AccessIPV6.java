package refatoracao.sistema.pojo;

import refatoracao.sistema.pojo.AccessPoint;

public class AccessIPV6 extends AccessPoint{

	IPV6 ipv6 = new IPV6();

	public void criarIPV6(String ip, String mask){
		ipv6.setEndereco(ip);
		ipv6.setMascara(mask);
	}
	public String direcionarIPV6(){
		return ipv6.direcionarIPV6(ipv6);
	}
	@Override
	public void criarIP(String ip, String mask) {
		criarIPV6(ip, mask);
	}
	@Override
	public String direcionarIP() {
		return direcionarIPV6();
	}
}
