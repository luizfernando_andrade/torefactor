package refatoracao.sistema.pojo;

public abstract class AccessPoint {

	public abstract void criarIP(String ip, String mask);
	
	public abstract String direcionarIP();
}
