package refatoracao.sistema.teste;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.sistema.pojo.AccessIPV4;
import refatoracao.sistema.pojo.AccessPoint;

public class TestarAccessIPV4 {

	@Test
	public void TestarDirecionamentoIPV4(){
		AccessPoint acesso = new AccessIPV4();
		acesso.criarIP("192.168.4.4", "255.255.255.0");
		
		Assert.assertTrue(acesso.direcionarIP().contains("IPV4"));
	}
}
