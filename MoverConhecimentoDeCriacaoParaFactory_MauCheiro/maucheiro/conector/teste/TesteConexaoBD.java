package maucheiro.conector.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.conector.pojo.Conector;
import maucheiro.conector.pojo.Conexao;
import maucheiro.conector.pojo.ConfiguraConexao;
import maucheiro.conector.pojo.TipoBaseDados;
import maucheiro.conector.pojo.TipoConexao;

public class TesteConexaoBD {

	@Test
	public void TestarConexaoCertificacao(){
		ConfiguraConexao configuraConexao = new ConfiguraConexao();
		
		configuraConexao.setUsuario("root");
		configuraConexao.setSenha("12345678909");
		configuraConexao.setBaseDados(TipoBaseDados.CERTIFICACAO);
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		Conector conector = new Conector(tipoConexao);
		
		Conexao conexao = conector.criarConexao();
		
		Assert.assertTrue(conexao.getUrlConexao().contains("cert"));
	}
}
