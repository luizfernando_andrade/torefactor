package maucheiro.conector.pojo;

public enum TipoBaseDados {

	CERTIFICACAO, HOMOLOGACAO, PRODUCAO, DESENVOLVIMENTO;
}
