package maucheiro.conector.pojo;

public class Conector {

	private TipoConector tipoConector;
	private TipoConexao tipoConexao;

	public Conector(TipoConexao tipoConexao) {
		this.tipoConexao = tipoConexao;
	}

	public TipoConexao getTipoConexao() {
		return tipoConexao;
	}

	public void setTipoConexao(TipoConexao tipoConexao) {
		this.tipoConexao = tipoConexao;
	}
	
	public Conexao criarConexao(){
		if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.CERTIFICACAO)){
			return new Conexao("Conectado a: certificacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.HOMOLOGACAO)){
			return new Conexao("Conectado a: homologacao/aplicacao");
		} else if (tipoConexao.getConfiguraConexao().getBaseDados()
				.equals(TipoBaseDados.PRODUCAO)){
			return new Conexao("Conectado a: producao/aplicacao");
		} else {
			return new Conexao("Conectado a: localhost/aplicacao");
		}
	}

	public TipoConector getTipoConector() {
		return tipoConector;
	}

	public void setTipoConector(TipoConector tipoConector) {
		this.tipoConector = tipoConector;
	}
	
	

}
