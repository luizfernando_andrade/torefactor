package refatoracao.pedidopizza.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.pedidopizza.pojo.MontarPedido;

public class TestarPedido {

	@Test
	public void pedidoDoisSabores50Cm(){
		MontarPedido pedido = new MontarPedido();
		pedido.setBordaChedar(true);
		pedido.setDoisRecheios(true);
		pedido.setSaborPizza("Marguerita");
		pedido.setSaborPizza2("Peperoni");
		pedido.setTamanho(50);
		
		String detalhamento = pedido.detalharPedido();
		
		Assert.assertTrue(detalhamento.contains("1 pizza m�dia gr�tis"));
	}
}
