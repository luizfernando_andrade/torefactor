package maucheiro.aluno.pojo;

import java.util.Date;

public class Aluno {
	private String nome;
	private Date nascimento;
	private Integer matricula;
	private Turma turma;
	private Dependencia dependencia;
	
	public Aluno(String nome, Date nascimento, Integer matricula){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
	}
	public Aluno(String nome, Date nascimento, Integer matricula,Turma turma){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
	}
	public Aluno(String nome, Date nascimento, Integer matricula,Turma turma, Dependencia dependencia){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
		this.dependencia = dependencia;
	}
	public Aluno(String nome, Date nascimento, Integer matricula, Dependencia dependencia){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.dependencia = dependencia;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getNascimento() {
		return nascimento;
	}
	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public Dependencia getDependencia() {
		return dependencia;
	}
	public void setDependencia(Dependencia dependencia) {
		this.dependencia = dependencia;
	}

	
}
