package maucheiro.arquivointerface.pojo;

import java.util.ArrayList;
import java.util.List;

public class Atributo {
	private Integer atributosLinha;
	private List<String> conteudo = new ArrayList<>();
	
	public Integer getAtributosLinha() {
		return atributosLinha;
	}
	public void setAtributosLinha(Integer atributosLinha) {
		this.atributosLinha = atributosLinha;
	}
	public List<String> getConteudo() {
		return conteudo;
	}
	public void adicionarConteudo(String conteudo) {
		this.conteudo.add(conteudo);
	}
}
