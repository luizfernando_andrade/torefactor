package maucheiro.arquivointerface.pojo;

public abstract class ArquivoInterfaceBuilder {

	public abstract String montarArquivo();
}
