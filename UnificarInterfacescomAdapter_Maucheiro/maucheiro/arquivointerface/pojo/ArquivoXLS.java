package maucheiro.arquivointerface.pojo;

import java.util.List;

public class ArquivoXLS extends ArquivoInterfaceBuilder{

	private Celula celula = new Celula();
	
	@Override
	public String montarArquivo() {
		String arquivo = "";
		int coluna = 0;
		for(int i = 0; i < celula.getValores().size(); i++){
			if (coluna == celula.getNumeroColunas()){
				arquivo.concat("\n");
			} else {
				arquivo.concat("|"+celula.getValores().get(i)+"|");
			}
		}
		return arquivo;
	}
	
	public void addCelula(String valorCelula) {
		celula.setValores(valorCelula);
	}
	
	public List<String> getCelulas() {
		return celula.getValores();
	}
}
