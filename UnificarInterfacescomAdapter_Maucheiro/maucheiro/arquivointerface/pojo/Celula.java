package maucheiro.arquivointerface.pojo;

import java.util.ArrayList;
import java.util.List;

public class Celula {

	private Integer numeroColunas;
	private List<String> valores = new ArrayList<>();
	
	public Integer getNumeroColunas() {
		return numeroColunas;
	}
	public void setNumeroColunas(Integer numeroColunas) {
		this.numeroColunas = numeroColunas;
	}
	public List<String> getValores() {
		return valores;
	}
	public void setValores(String valores) {
		this.valores.add(valores);
	}
}
