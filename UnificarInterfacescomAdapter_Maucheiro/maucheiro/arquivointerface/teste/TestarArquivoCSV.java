package maucheiro.arquivointerface.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.arquivointerface.pojo.ArquivoCSV;
import maucheiro.arquivointerface.pojo.Atributo;

public class TestarArquivoCSV {

	@Test
	public void montarArquivoCSV(){
		ArquivoCSV arquivo = new ArquivoCSV();
		
		arquivo.addAtributo("teste");
		arquivo.addAtributo("Teste2");
		arquivo.addAtributo("Teste3");
		arquivo.addAtributo("Teste4");
		arquivo.getAtributo().setAtributosLinha(2);
		arquivo.montarArquivo();
		
		Assert.assertTrue(arquivo.getAtributo() instanceof Atributo);
		
		
		
	}
}
