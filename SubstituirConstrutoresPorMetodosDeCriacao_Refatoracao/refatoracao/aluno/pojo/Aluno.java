package refatoracao.aluno.pojo;

import java.util.Date;

public class Aluno {
	private String nome;
	private Date nascimento;
	private Integer matricula;
	private Turma turma;
	private Dependencia dependencia;
	
	public static Aluno criarAlunoComDependencia(String nome, Date nascimento, 
			Integer matricula, Dependencia dependencia){
		return new Aluno(nome, nascimento, matricula, null, dependencia);
	}
	public static Aluno criarAlunoComDependenciaETurma(String nome, Date nascimento, 
			Integer matricula,Turma turma, Dependencia dependencia){
		return new Aluno(nome, nascimento, matricula, turma, dependencia);
	}
	public static Aluno criarAlunoComTurma(String nome, Date nascimento, 
			Integer matricula,Turma turma){
		return new Aluno(nome, nascimento, matricula, turma, null);
	}
	public static Aluno criarAluno(String nome, Date nascimento, 
			Integer matricula){
		return new Aluno(nome, nascimento, matricula, null, null);
	}
	
	private Aluno(String nome, Date nascimento, Integer matricula,
			Turma turma, Dependencia dependencia){
		this.nome = nome;
		this.nascimento = nascimento;
		this.matricula = matricula;
		this.turma = turma;
		this.dependencia = dependencia;
	}

	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Date getNascimento() {
		return nascimento;
	}
	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}
	public Integer getMatricula() {
		return matricula;
	}
	public void setMatricula(Integer matricula) {
		this.matricula = matricula;
	}
	public Turma getTurma() {
		return turma;
	}
	public void setTurma(Turma turma) {
		this.turma = turma;
	}
	public Dependencia getDependencia() {
		return dependencia;
	}
	public void setDependencia(Dependencia dependencia) {
		this.dependencia = dependencia;
	}

	
}
