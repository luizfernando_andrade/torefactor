package refatoracao.pedido.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.pedido.pojo.Encomenda;
import refatoracao.pedido.pojo.EnumSituacao;

public class TestarMudancaEstado {

	@Test
	public void mudarEstado(){
		Encomenda encomenda = new Encomenda();
		
		encomenda.setSituacao(EnumSituacao.SEPARACAO);
		
		encomenda.mudarSituacao();
		
		Assert.assertTrue(encomenda.getSituacao()
				.equals(EnumSituacao.TRANSPORTADORA));
	}
}
