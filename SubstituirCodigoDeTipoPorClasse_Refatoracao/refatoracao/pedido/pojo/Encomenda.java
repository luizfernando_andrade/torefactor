package refatoracao.pedido.pojo;

public class Encomenda {

	EnumSituacao situacao;
	

	
	public EnumSituacao mudarSituacao(){
		if (situacao.equals(EnumSituacao.SEPARACAO)){
			situacao = EnumSituacao.TRANSPORTADORA;
		} else if (situacao.equals(EnumSituacao.TRANSPORTADORA)) {
			situacao = EnumSituacao.CORREIOS;
		} else {
			situacao = EnumSituacao.ENTREGUE;
		}
		return situacao;
	}



	public EnumSituacao getSituacao() {
		return situacao;
	}



	public void setSituacao(EnumSituacao situacao) {
		this.situacao = situacao;
	}
	
}
