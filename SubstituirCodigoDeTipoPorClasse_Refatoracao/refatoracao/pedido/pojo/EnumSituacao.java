package refatoracao.pedido.pojo;

public enum EnumSituacao {


	SEPARACAO("Separacao"), 
	TRANSPORTADORA("Transportadora"),
	CORREIOS("Correios"),
	ENTREGUE("Entregue");
	
	private String string;

	private EnumSituacao(String string) {
		this.string = string;
	}
	
	public String toString(){
		return string;
	}
	
}
