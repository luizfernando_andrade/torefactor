package maucheiro.decorator.pojo;

public class CursoIntensivo extends Curso{

	private String diaAulaExtra;

	@Override
	public String infoCurso() {
		return "Curso pr� vestibular intensivo, 6 meses de dura��o";
	}

	@Override
	public String prepararAula() {
		return "Conte�do sintetizado para o vestibular";
	}

	@Override
	public String avaliacaoSemanal() {
		return "Conte�do acumulativopara " + tipoCurso();
	}

	@Override
	public String tipoCurso() {
		return "Curso Intensivo";
	}
	
	//m�todos adicionais
	public void aulaExtra(String aulaExtra){
		if (aulaExtra != null){
			diaAulaExtra = "aula extra " + aulaExtra;
		}
	}
	public String infoBasica(){
		return tipoCurso() + " " + getDiaAulaExtra();
	}
	public String getDiaAulaExtra() {
		return diaAulaExtra;
	}
	public void setDiaAulaExtra(String diaAulaExtra) {
		this.diaAulaExtra = diaAulaExtra;
	}

}
