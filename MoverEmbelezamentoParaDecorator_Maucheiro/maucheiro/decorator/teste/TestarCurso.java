package maucheiro.decorator.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.decorator.pojo.CursoIntensivo;

public class TestarCurso {

	@Test
	public void testarCursoIntensivo(){
		String aulaExtra = "sabado";
		
		CursoIntensivo cursoIntensivo = new CursoIntensivo();
		
		cursoIntensivo.aulaExtra(aulaExtra);
		
		String informacaoCurso = cursoIntensivo.infoBasica();
		
		Assert.assertTrue(informacaoCurso.contains("extra"));
	}
}
