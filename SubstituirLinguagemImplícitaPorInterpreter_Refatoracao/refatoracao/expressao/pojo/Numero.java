package refatoracao.expressao.pojo;

public class Numero implements Expressao {

	private int numero;

	public Numero(int numero) {
		this.numero = numero;
	}
	
	@Override
	public int avaliarExpressao() {
		return numero;
	}

}
