package refatoracao.expressao.pojo;

public class Soma implements Expressao{

	private Expressao esquerda;
	private Expressao direita;
	public Soma(Expressao esquerda, Expressao direita) {
		this.esquerda = esquerda;
		this.direita = direita;
	}
	@Override
	public int avaliarExpressao() {
		return esquerda.avaliarExpressao()
				+ direita.avaliarExpressao();
	}

}
