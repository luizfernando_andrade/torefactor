package refatoracao.expressao.pojo;

public class Subtracao implements Expressao {

	private Expressao direita;
	private Expressao esqueda;
	public Subtracao(Expressao direita, Expressao esqueda) {
		this.direita = direita;
		this.esqueda = esqueda;
	}
	@Override
	public int avaliarExpressao() {
		return direita.avaliarExpressao() 
				- esqueda.avaliarExpressao();
	}

}
