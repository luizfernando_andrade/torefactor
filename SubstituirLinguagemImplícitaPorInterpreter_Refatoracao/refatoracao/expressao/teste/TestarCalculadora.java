package refatoracao.expressao.teste;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.expressao.pojo.Expressao;
import refatoracao.expressao.pojo.Numero;
import refatoracao.expressao.pojo.Soma;


public class TestarCalculadora {

	@Test
	public void testarSoma(){
		
		Expressao soma = new Soma(new Numero(10), new Numero(7));
		int resultado = soma.avaliarExpressao();
		
		Assert.assertTrue(resultado == 17);
	}
}
