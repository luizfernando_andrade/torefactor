package maucheiro.pojo;

public enum TipoFuncionario {
	OPERACIONAL, 
	COORDENADOR, 
	GERENTE;
}
