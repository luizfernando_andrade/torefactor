package maucheiro.pojo;

import java.util.Calendar;

public class Funcionario {

	private Pessoa pessoa;
	private TipoFuncionario tipoFuncionario;
	private Salario salario;
	private Calendar admissao;
	
	public Pessoa getPessoa() {
		return pessoa;
	}
	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}
	public TipoFuncionario getTipoFuncionario() {
		return tipoFuncionario;
	}
	public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
		this.tipoFuncionario = tipoFuncionario;
	}
	public Double getSalario() {
		return salario.getValorSalario();
	}
	public void setSalario(Salario salario) {
		this.salario = salario;
	}
	public Calendar getAdmissao() {
		return admissao;
	}
	public void setAdmissao(Calendar admissao) {
		this.admissao = admissao;
	}
	
	public Integer tempoDeCasaEmAnos(){
		
		Integer anoAtual = Calendar.getInstance().get(Calendar.YEAR);
		Integer anoAdmissao = admissao.get(Calendar.YEAR);
		
		return anoAtual - anoAdmissao;
	}

	
	public Double calcularSalarioComComissao(){
		
		if (TipoFuncionario.GERENTE.equals(tipoFuncionario)){
			return getSalario() + (getSalario() * 0.3) + adicionalGerente();
		} // adiniona 30% ao sal�rio + 5% ao ano  
		else if (TipoFuncionario.COORDENADOR.equals(tipoFuncionario)){
			return getSalario() + (getSalario() * 0.2);
		} // adiniona 20% ao sal�rio 
		else {
			return getSalario() + (getSalario() * 0.1);
		} // adiniona 10% ao sal�rio
	}
	private Double adicionalGerente(){
		return ((tempoDeCasaEmAnos() * 5) / 100) * getSalario();
	}
	
}
