package maucheiro.teste;
import java.util.Calendar;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.pojo.Funcionario;
import maucheiro.pojo.Pessoa;
import maucheiro.pojo.Salario;
import maucheiro.pojo.TipoFuncionario;

public class TesteCalculoSalario {

	@Test
	public void test() {
		//Dados Pessoa
		Pessoa pessoa = new Pessoa();
		Calendar nascimento = Calendar.getInstance();
		nascimento.set(1980, 10, 15);
		
		pessoa.setDocumento("123456789");
		pessoa.setNascimento(nascimento);
		pessoa.setNome("Juliana Maria");
		
		//Dados Funcionario
		Calendar admissao = Calendar.getInstance();
		admissao.set(2010, 10, 15);
		Salario salario = new Salario();
		salario.setValorSalario(11000.00);
		
		Funcionario funcionario = new Funcionario();
		funcionario.setAdmissao(admissao);
		funcionario.setPessoa(pessoa);
		funcionario.setSalario(salario);
		funcionario.setTipoFuncionario(TipoFuncionario.COORDENADOR);
		
		Assert.assertTrue(funcionario.calcularSalarioComComissao().equals(new Double(13200.00)));
	}

}
