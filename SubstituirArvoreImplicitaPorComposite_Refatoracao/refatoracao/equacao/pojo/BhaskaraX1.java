package refatoracao.equacao.pojo;

public class BhaskaraX1 {
	
	private DeltaComposite delta;
	private Double a;
	private Double b;

	public BhaskaraX1(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}
	
	public double calcularBhaskaraX1() {
		return (-b + Math.sqrt( delta.calcularDelta()))/2*a;
	}
}
