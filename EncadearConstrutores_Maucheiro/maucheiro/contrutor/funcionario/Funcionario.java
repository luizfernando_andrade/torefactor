package maucheiro.contrutor.funcionario;

import java.util.Date;
import java.util.List;

public class Funcionario {
	
	private String nome;
	private String cpf;
	private Date nascimento;
	private List<Dependente> dependentes;
	private boolean possuiCNH;
	
	public Funcionario( String nome, String cpf, Date nascimento, 
			List<Dependente> dependentes, boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
		this.possuiCNH = possuiCNH;
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			List<Dependente> dependentes) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
	}
	public Funcionario( String nome, String cpf, Date nascimento) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.possuiCNH = possuiCNH;
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getNascimento() {
		return nascimento;
	}
	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}
	public List<Dependente> getDependentes() {
		return dependentes;
	}
	public void setDependentes(List<Dependente> dependentes) {
		this.dependentes = dependentes;
	}

	public boolean isPossuiCNH() {
		return possuiCNH;
	}

	public void setPossuiCNH(boolean possuiCNH) {
		this.possuiCNH = possuiCNH;
	}
	

}
