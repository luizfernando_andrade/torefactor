package maucheiro.sistema.teste;

import org.junit.Test;

import junit.framework.Assert;
import maucheiro.sistema.pojo.AcessPoint;

public class TestarAcessIPV4 {

	@Test
	public void TestarDirecionamentoIPV4(){
		AcessPoint acesso = new AcessPoint();
		acesso.criarIPV4("192.168.4.4", "255.255.255.0");
		
		Assert.assertTrue(acesso.direcionarIP().contains("IPV4"));
	}
}
