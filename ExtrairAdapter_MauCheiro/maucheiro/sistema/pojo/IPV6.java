package maucheiro.sistema.pojo;

public class IPV6 {

	private String endereco;
	private String mascara;
	
	public String direcionarIPV6(IPV6 ipv6){
		return "Redirect to: " + ipv6.getEndereco();
	}
	
	public String enviarPacoteViaIPV6(){
		return "Enviando via IPV6";
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getMascara() {
		return mascara;
	}
	public void setMascara(String mascara) {
		this.mascara = mascara;
	}
}
