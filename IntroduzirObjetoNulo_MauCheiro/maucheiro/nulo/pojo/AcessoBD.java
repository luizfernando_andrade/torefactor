package maucheiro.nulo.pojo;

public class AcessoBD {

	private Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public boolean realizarLogin(String login,String senha){
		if (usuario != null){
			return usuario.logar(login,senha);
		}
		return true;
	}
}
