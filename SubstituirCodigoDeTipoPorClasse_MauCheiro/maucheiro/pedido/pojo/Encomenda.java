package maucheiro.pedido.pojo;

public class Encomenda {

	private String situacao;
	private static final String SEPARACAO = "Separacao";
	private static final String TRANSPORTADORA = "Transportadora";
	private static final String CORREIOS = "Correios";
	private static final String ENTREGUE = "Entregue";
	
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	
	public String mudarSituacao(){
		if (situacao.equals(SEPARACAO)){
			situacao = TRANSPORTADORA;
		} else if (situacao.equals(TRANSPORTADORA)) {
			situacao = CORREIOS;
		} else {
			situacao = ENTREGUE;
		}
		return situacao;
	}
	public static String getSeparacao() {
		return SEPARACAO;
	}
	public static String getTransportadora() {
		return TRANSPORTADORA;
	}
	public static String getCorreios() {
		return CORREIOS;
	}
	public static String getEntregue() {
		return ENTREGUE;
	}
	
}
