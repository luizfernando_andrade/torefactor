package maucheiro.pedido.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.pedido.pojo.Encomenda;

public class TestarMudancaEstado {

	@Test
	public void mudarEstado(){
		Encomenda encomenda = new Encomenda();
		
		encomenda.setSituacao(Encomenda.getSeparacao());
		
		encomenda.mudarSituacao();
		
		Assert.assertTrue(encomenda.getSituacao()
				.equals(Encomenda.getTransportadora()));
	}
}
