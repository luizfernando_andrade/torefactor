package refatoracao.componente.pojo;

public abstract class Componente {

	public static ComponenteEletronico criarComponenteEletronico(String nome) {
		return new ComponenteEletronico(nome);
	}
	public static Componente criarComponenteMecanico(String nome) {
		return new ComponenteMecanico(nome);
	}
	public static Componente criarComponentePeriferico(String nome) {
		return new ComponentePeriferico(nome);
	}
	
	private String nome;
	
	protected abstract String tipoComponente();

	public Componente(String nome) {
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
