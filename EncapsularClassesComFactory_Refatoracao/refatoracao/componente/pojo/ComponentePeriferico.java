package refatoracao.componente.pojo;

public class ComponentePeriferico extends Componente {

	protected ComponentePeriferico(String nome) {
		super(nome);
	}
	@Override
	protected String tipoComponente() {
		return "Componente Periférico";
	}

}
