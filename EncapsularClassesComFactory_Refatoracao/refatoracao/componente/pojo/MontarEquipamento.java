package refatoracao.componente.pojo;

import java.util.List;

public class MontarEquipamento {

	private String descricao;
	private List<Componente> componentes;
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public List<Componente> getComponentes() {
		return componentes;
	}
	public void setComponentes(List<Componente> componentes) {
		this.componentes = componentes;
	}
}
