package refatoracao.componente.pojo;

public class ComponenteEletronico extends Componente {

	protected ComponenteEletronico(String nome) {
		super(nome);
	}
	@Override
	protected String tipoComponente() {
		return "Componente Eletrônico";
	}

}
