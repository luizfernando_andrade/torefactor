package refatoracao.componente.teste;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.componente.pojo.Componente;
import refatoracao.componente.pojo.MontarEquipamento;

public class TestarMontagemEquipamento {

	@Test
	public void testarMontagem(){
		MontarEquipamento equipamento = new MontarEquipamento();
		List<Componente> componentes = new ArrayList<>();
		
		componentes.add(Componente.criarComponenteEletronico("fuz�vel"));
		componentes.add(Componente.criarComponenteMecanico("Alavanca disjuntor"));
		componentes.add(Componente.criarComponentePeriferico("Cabo de alta tens�o"));
		
		equipamento.setDescricao("Painel el�trico");
		equipamento.setComponentes(componentes);
		
		Assert.assertTrue(equipamento.getComponentes() != null
						  && equipamento.getComponentes().size() == 3);
	}
}
