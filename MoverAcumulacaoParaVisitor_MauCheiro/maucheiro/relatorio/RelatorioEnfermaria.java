package maucheiro.relatorio;

import maucheiro.arquivo.TipoArquivo;
import maucheiro.arquivo.TipoArquivoHTML;
import maucheiro.arquivo.TipoArquivoXML;

public class RelatorioEnfermaria implements Relatorio {

	private String cabecalho;
	private String corpo;
	private String rodape;

	
	public String imprimir(TipoArquivo tipoArquivo){
		if (tipoArquivo instanceof TipoArquivoHTML){
			return ((TipoArquivoHTML) tipoArquivo).imprimirHTML(this);
		}
		if (tipoArquivo instanceof TipoArquivoXML) {
			return ((TipoArquivoXML) tipoArquivo).imprimirXML(this);
		}
		return null; 
		
	}
	
	@Override
	public void cabecalho(String cabecalho) {
		this.cabecalho = "Relatorio Enfermaria: " + cabecalho;
	}

	@Override
	public void corpo(String corpo) {
		this.corpo = "Resumo de turno enfermeiro(a)";
	}

	@Override
	public void rodape(String rodape) {
		this.rodape = "N�MERO COREN: " + rodape;
	}

	public String getCabecalho() {
		return cabecalho;
	}

	public void setCabecalho(String cabecalho) {
		this.cabecalho = cabecalho;
	}

	public String getCorpo() {
		return corpo;
	}

	public void setCorpo(String corpo) {
		this.corpo = corpo;
	}

	public String getRodape() {
		return rodape;
	}

	public void setRodape(String rodape) {
		this.rodape = rodape;
	}

}
