package maucheiro.relatorio;

import maucheiro.arquivo.TipoArquivo;

public interface Relatorio {
	
	void cabecalho(String cabecalho);
	void corpo(String corpo);
	void rodape(String rodape);
	String imprimir(TipoArquivo tipoArquivo);
	
}
