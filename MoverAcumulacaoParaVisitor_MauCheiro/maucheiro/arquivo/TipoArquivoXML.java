package maucheiro.arquivo;

import maucheiro.relatorio.RelatorioEnfermaria;
import maucheiro.relatorio.RelatorioMedico;

public class TipoArquivoXML implements TipoArquivo {

	String resultado = null;
	
	@Override
	public void printCabecalho(String t) {
		resultado.concat("<cabecalho>" + t + "</cabecalho>");

	}

	@Override
	public void printCorpo(String t) {
		resultado.concat("<corpo>" + t + "</corpo>");

	}

	@Override
	public void printRodape(String t) {
		resultado.concat("<rodape>" + t + "</rodape>");

	}

	public String imprimirXML(RelatorioEnfermaria relatorioEnfermaria) {
		
		printCabecalho(relatorioEnfermaria.getCabecalho());
		printCorpo(relatorioEnfermaria.getCorpo());
		printRodape(relatorioEnfermaria.getRodape());
		
		return "<?xml version='1.0' encoding='ISO-8859-1'?>" + resultado;
	}
	public String imprimirXML(RelatorioMedico relatorioMedico) {
		
		printCabecalho(relatorioMedico.getCabecalho());
		printCorpo(relatorioMedico.getCorpo());
		printRodape(relatorioMedico.getRodape());
		
		return "<?xml version='1.0' encoding='ISO-8859-1'?>" + resultado;
	}

}
