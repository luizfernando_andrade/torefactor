package maucheiro.arquivo;

import maucheiro.relatorio.RelatorioEnfermaria;
import maucheiro.relatorio.RelatorioMedico;

public class TipoArquivoHTML implements TipoArquivo {

	StringBuffer sb = new StringBuffer();
	String resultado = new String(); 
	
	@Override
	public void printCabecalho(String t) {
		sb.append("<head>" + t + "</head>");
		
	}

	@Override
	public void printCorpo(String t) {
		sb.append("<body>" + t + "</body>");
		
	}

	@Override
	public void printRodape(String t) {
		sb.append("<footer>" + t + "</footer>");
		
	}

	public String imprimirHTML(RelatorioEnfermaria relatorioEnfermaria) {
		
		printCabecalho(relatorioEnfermaria.getCabecalho());
		printCorpo(relatorioEnfermaria.getCorpo());
		printRodape(relatorioEnfermaria.getRodape());
		
		return "<html>" + sb.toString() + "</html>";
	}
	
	public String imprimirHTML(RelatorioMedico relatorioMedico) {
		
		printCabecalho(relatorioMedico.getCabecalho());
		printCorpo(relatorioMedico.getCorpo());
		printRodape(relatorioMedico.getRodape());
		
		return "<html>" + resultado + "</html>";
	}

}
