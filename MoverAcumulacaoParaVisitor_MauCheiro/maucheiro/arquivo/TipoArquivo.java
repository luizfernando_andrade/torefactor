package maucheiro.arquivo;

public interface TipoArquivo {

	void printCabecalho(String t);
	void printCorpo(String t);
	void printRodape(String t);
}
