package maucheiro.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.arquivo.TipoArquivo;
import maucheiro.arquivo.TipoArquivoHTML;
import maucheiro.relatorio.Relatorio;
import maucheiro.relatorio.RelatorioEnfermaria;

public class TestarRelatorioHTML {

	@Test
	public void testarRelatorioPDF(){
		TipoArquivo pdf = new TipoArquivoHTML();
		Relatorio relatorio = new RelatorioEnfermaria();
		relatorio.cabecalho("Meu Deus");
		relatorio.corpo("Esse trem tem que funcionar");
		relatorio.rodape("12345");
		Assert.assertTrue(relatorio.imprimir(pdf).contains("<html>"));
	}
}
