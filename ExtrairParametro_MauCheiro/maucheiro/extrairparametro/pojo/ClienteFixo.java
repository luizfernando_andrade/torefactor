package maucheiro.extrairparametro.pojo;

public class ClienteFixo {

	private Double valorCompra;
	private Double valorDesconto;
	public ClienteFixo(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
	
	public Double getValorCompra() {
		return valorCompra;
	}

	public void setValorCompra(Double valorCompra) {
		this.valorCompra = valorCompra;
	}

	public Double calcularDesconto(Double compra){
		return compra - valorDesconto;
	}
}
