package maucheiro.biblioteca.pojo;

public class Livro {

	private String titulo;
	private EstadoLivro estadoLivro;
	private NotificarReserva notificacao = new NotificarReserva();;
	
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public EstadoLivro getEstadoLivro() {
		return estadoLivro;
	}
	public void setEstadoLivro(EstadoLivro estadoLivro) {
		this.estadoLivro = estadoLivro;
	}
	public NotificarReserva getNotificacao() {
		return notificacao;
	}
	public void setNotificacao(NotificarReserva notificacao) {
		this.notificacao = notificacao;
	}
	
	public String devolverLivro(){
		String mensagem = notificacao.notifica(this);
		setEstadoLivro(EstadoLivro.liberado);
		return mensagem;
	}
}
