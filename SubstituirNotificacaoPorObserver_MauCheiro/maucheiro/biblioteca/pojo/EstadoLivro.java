package maucheiro.biblioteca.pojo;

public enum EstadoLivro {

	liberado, reservado, emprestado;
}
