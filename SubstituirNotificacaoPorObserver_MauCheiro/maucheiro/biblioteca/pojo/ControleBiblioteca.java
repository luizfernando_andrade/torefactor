package maucheiro.biblioteca.pojo;

public class ControleBiblioteca {

	private String log;
	
	public void devolucao(Livro livro){
		log = livro.devolverLivro();
	}
	
	public String getLog() {
		return log;
	}
	public void setLog(String log) {
		this.log = log;
	}
}
