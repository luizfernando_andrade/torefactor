package maucheiro.biblioteca.pojo;

public class NotificarReserva {

	private String mensagem = null;
	
	public String notifica(Livro livro){
		if (EstadoLivro.reservado.equals(livro.getEstadoLivro())){
			mensagem = "Livro possui reserva";
			System.out.println(mensagem);
		}
		return mensagem;
	}
}
