package maucheiro.biblioteca.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.biblioteca.pojo.ControleBiblioteca;
import maucheiro.biblioteca.pojo.EstadoLivro;
import maucheiro.biblioteca.pojo.Livro;

public class TesteEmprestimo {

	@Test
	public void testarEmprestimo(){
	
		Livro livro = new Livro();
		livro.setEstadoLivro(EstadoLivro.reservado);
		livro.setTitulo("LivroTeste");
		
		ControleBiblioteca controle = new ControleBiblioteca();
		controle.devolucao(livro);
		
		System.out.println(controle.getLog());
		
		Assert.assertTrue(EstadoLivro.liberado.equals(livro.getEstadoLivro()));
	}
}
