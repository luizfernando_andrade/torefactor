package maucheiro.pedidopizza.pojo;

public class MontarPedido {

	private String saborPizza;
	private String saborPizza2;
	private boolean bordaChedar;
	private boolean doisRecheios;
	private Integer tamanho;

	StringBuffer pedido = new StringBuffer();

	public String detalharPedido(){
		pedido.append(saborPizza);
		
		if(doisRecheios){
			pedido.append(" metade: ")
			.append(saborPizza2);
		} 
		if(bordaChedar){
			pedido.append(" com borda de chedar");
		} 
		if (tamanho > 45) {
			pedido.append(" 45cm + 1 pizza m�dia gr�tis");
		} else {
			pedido.append(tamanho.toString());
		}
		return pedido.toString();

	}

	public String getSaborPizza() {
		return saborPizza;
	}

	public void setSaborPizza(String saborPizza) {
		this.saborPizza = saborPizza;
	}

	public String getSaborPizza2() {
		return saborPizza2;
	}

	public void setSaborPizza2(String saborPizza2) {
		this.saborPizza2 = saborPizza2;
	}

	public boolean isBordaChedar() {
		return bordaChedar;
	}

	public void setBordaChedar(boolean bordaChedar) {
		this.bordaChedar = bordaChedar;
	}

	public boolean isDoisRecheios() {
		return doisRecheios;
	}

	public void setDoisRecheios(boolean doisRecheios) {
		this.doisRecheios = doisRecheios;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	} 
}
