package refatoracao.nulo.pojo;

public class Usuario {

	private String login;
	private String senha;
	private String usuarioLogado;
	
	public Usuario(Object object) {	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	
	public boolean logar(String usuario, String senha){
		usuarioLogado = usuario;
		return true;
	}
	public String getUsuarioLogado() {
		return usuarioLogado;
	}
	public void setUsuarioLogado(String usuarioLogado) {
		this.usuarioLogado = usuarioLogado;
	}
}
