package refatoracao.curso.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.curso.pojo.Curso;
import refatoracao.curso.pojo.CursoExtensivo;
import refatoracao.curso.pojo.CursoIntensivo;

public class TesteCursoPreVestibular {

	Curso curso = null;
	
	@Test
	public void testarCursoIntensivo(){
		curso = new CursoIntensivo();
		String descricaoCurso = curso.descricaoCurso();
		
		Assert.assertTrue(descricaoCurso.
				contains("Intensivo"));
	}
	
	@Test
	public void testarCursoExtensivo(){
		curso = new CursoExtensivo();
		String descricaoCurso = curso.descricaoCurso();
		
		Assert.assertTrue(descricaoCurso.
				contains("Extensivo"));
	}
}
