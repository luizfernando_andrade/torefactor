package refatoracao.curso.pojo;

public abstract class Curso {
	
	public abstract String infoCurso();
	
	public abstract String prepararAula();
	
	public abstract String avaliacaoSemanal();
	
	public abstract String tipoCurso();

	public String descricaoCurso() {
		
		return infoCurso()
		.concat(prepararAula())
		.concat(avaliacaoSemanal());
	}
	
}
