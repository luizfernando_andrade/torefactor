package refatoracao.pedido.pojo;

import java.util.List;

public class ManipularPedido {

	private CompositePedido composite;

	public ManipularPedido(CompositePedido composite) {
		this.composite = composite;
	}
	
	public List<Pedido> liberarPedido(){
		return composite.liberar();
	}


}
