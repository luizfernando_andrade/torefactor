package refatoracao.pedido.pojo;

import java.util.ArrayList;
import java.util.List;

public class ManipularUnicoPedidoComposite implements CompositePedido{

	private Pedido pedido;

	public ManipularUnicoPedidoComposite(Pedido pedido) {
		this.pedido = pedido;
	}
	
	public List<Pedido> liberar() {
		List<Pedido> pedidos = new ArrayList<Pedido>();
		pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		pedidos.add(pedido);
		return pedidos;
	}
}
