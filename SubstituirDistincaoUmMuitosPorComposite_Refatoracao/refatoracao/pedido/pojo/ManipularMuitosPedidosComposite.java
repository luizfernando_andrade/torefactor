package refatoracao.pedido.pojo;

import java.util.List;

public class ManipularMuitosPedidosComposite implements CompositePedido {

	private List<Pedido> pedidos;

	public ManipularMuitosPedidosComposite(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}
	
	public List<Pedido> liberar() {
		for (Pedido pedido : pedidos) {
			pedido.setSituacaoPedido(SituacaoPedido.Liberado);
		}
		return pedidos;
	}
}
