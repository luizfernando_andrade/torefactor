package refatoracao.pedido.pojo;

import java.util.List;

public interface CompositePedido {

	public List<Pedido> liberar();
}