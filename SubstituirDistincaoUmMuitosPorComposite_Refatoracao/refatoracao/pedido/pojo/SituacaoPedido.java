package refatoracao.pedido.pojo;

public enum SituacaoPedido {
	Pendente, Cancelado, Liberado;
}
