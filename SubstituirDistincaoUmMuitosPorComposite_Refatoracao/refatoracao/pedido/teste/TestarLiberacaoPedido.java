package refatoracao.pedido.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.pedido.pojo.Item;
import refatoracao.pedido.pojo.ManipularMuitosPedidosComposite;
import refatoracao.pedido.pojo.ManipularPedido;
import refatoracao.pedido.pojo.ManipularUnicoPedidoComposite;
import refatoracao.pedido.pojo.Pedido;
import refatoracao.pedido.pojo.SituacaoPedido;

public class TestarLiberacaoPedido {

	@Test
	public void liberarPedidoUnico(){
		boolean pedidoPendente = false;
		
		Pedido pedido = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		
		List<Item> itens = new ArrayList<Item>();
		itens.add(new Item("protetor auricular"));
		
		pedido.setItens(itens);
		ManipularUnicoPedidoComposite unicoPedidoManipulador = 
				new ManipularUnicoPedidoComposite(pedido);
		ManipularPedido manipularPedido = new ManipularPedido(unicoPedidoManipulador);
		
		for (Pedido unidade : manipularPedido.liberarPedido()) {
			if (unidade.getSituacaoPedido().equals(SituacaoPedido.Pendente)){
				pedidoPendente = true;
			}
		} 
		
		Assert.assertTrue(!pedidoPendente);
	}
	
	@Test
	public void liberarPedidos(){
		boolean pedidoPendente = false;
		List<Item> itens = new ArrayList<Item>();
		itens.add(new Item("protetor auricular"));
		itens.add(new Item("protetor bucal"));
		itens.add(new Item("protetor nasal"));
		
		List<Pedido> pedidos = new ArrayList<>();
		
		Pedido pedido = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido);
		Pedido pedido2 = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido2);
		Pedido pedido3 = new Pedido(SituacaoPedido.Pendente);
		pedido.setDataPedido(Calendar.getInstance());
		pedido.setItens(itens);
		pedidos.add(pedido3);
		
		ManipularMuitosPedidosComposite muitosPedidosManipulador = 
				new ManipularMuitosPedidosComposite(pedidos);
		ManipularPedido manipularPedido = new ManipularPedido(muitosPedidosManipulador);
		
		for (Pedido unidade : manipularPedido.liberarPedido()) {
			if (unidade.getSituacaoPedido().equals(SituacaoPedido.Pendente)){
				pedidoPendente = true;
			}
		} 
		Assert.assertTrue(!pedidoPendente);
	}
	
}
