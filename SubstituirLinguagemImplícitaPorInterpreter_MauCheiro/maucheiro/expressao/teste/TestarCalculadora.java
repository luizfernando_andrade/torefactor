package maucheiro.expressao.teste;

import org.junit.Test;

import junit.framework.Assert;
import maucheiro.expressao.pojo.Expressao;


public class TestarCalculadora {

	@Test
	public void testarSoma(){
		
		Expressao expressao = new Expressao();
		expressao.setNumDireita(12);
		expressao.setNumEsquerda(5);
		expressao.setExpressaoMatematica("+");
		int resultado = expressao.avaliarExpressao();
		
		Assert.assertTrue(resultado == 17);
	}
}
