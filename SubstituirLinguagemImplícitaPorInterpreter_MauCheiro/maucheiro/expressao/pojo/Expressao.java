package maucheiro.expressao.pojo;

public class Expressao {

	private int numDireita;
	private int numEsquerda;
	private String expressaoMatematica;
	
	public int avaliarExpressao(){
		if (expressaoMatematica.equals("+")){
			return numEsquerda + numDireita;
		}
		if (expressaoMatematica.equals("-")){
			return numEsquerda - numDireita;
		}
		return 0;
	}
	public int getNumDireita() {
		return numDireita;
	}
	public void setNumDireita(int numDireita) {
		this.numDireita = numDireita;
	}
	public int getNumEsquerda() {
		return numEsquerda;
	}
	public void setNumEsquerda(int numEsquerda) {
		this.numEsquerda = numEsquerda;
	}
	public String getExpressaoMatematica() {
		return expressaoMatematica;
	}
	public void setExpressaoMatematica(String expressaoMatematica) {
		this.expressaoMatematica = expressaoMatematica;
	}
}
