package maucheiro.conexao.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.conexao.pojo.DataBaseConnection;


public class TestarConexao {

	@Test
	public void TestarDuasInstanciasBD(){
		DataBaseConnection conexaoBD1 = new DataBaseConnection();
		
		conexaoBD1.conectarBase();
		
		conexaoBD1.recuperarRegistros();
		
		conexaoBD1.desconectarBase();
		
		DataBaseConnection conexaoBD2 = new DataBaseConnection();
		
		conexaoBD2.conectarBase();
		
		conexaoBD2.recuperarRegistros();
		
		conexaoBD2.desconectarBase();
		
		Assert.assertTrue(!conexaoBD1.equals(conexaoBD2));
	}
}
