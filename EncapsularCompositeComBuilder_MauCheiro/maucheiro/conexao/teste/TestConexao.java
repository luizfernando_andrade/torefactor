package maucheiro.conexao.teste;

import org.junit.Assert;
import org.junit.Test;

import maucheiro.conexao.pojo.Conector;
import maucheiro.conexao.pojo.Conexao;
import maucheiro.conexao.pojo.ConfiguraConexao;
import maucheiro.conexao.pojo.TipoBaseDados;
import maucheiro.conexao.pojo.TipoConexao;

public class TestConexao {
	
	@Test
	public void testarConexao(){
		ConfiguraConexao configuraConexao = 
				new ConfiguraConexao("admin","abcd01",TipoBaseDados.PRODUCAO);
		
		TipoConexao tipoConexao = new TipoConexao(configuraConexao);
		
		Conector conector = new Conector(tipoConexao);
		
		Conexao conexao = conector.criarConexao();
		
		Assert.assertTrue(conexao != null);
	}
}
