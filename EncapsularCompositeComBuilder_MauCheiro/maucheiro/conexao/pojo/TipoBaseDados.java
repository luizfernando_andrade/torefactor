package maucheiro.conexao.pojo;

public enum TipoBaseDados {

	CERTIFICACAO, HOMOLOGACAO, PRODUCAO, DESENVOLVIMENTO;
}
