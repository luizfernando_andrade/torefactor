package maucheiro.driver.teste;

import maucheiro.driver.pojo.FirefoxDriver;
import maucheiro.driver.pojo.WebDriver;

public class TestDriverFirefox implements TestDriver{

	@Override
	public String direcionarURL(String url) {
		
		WebDriver driver = new FirefoxDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}

}
