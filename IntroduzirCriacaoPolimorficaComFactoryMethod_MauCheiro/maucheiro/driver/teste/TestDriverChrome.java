package maucheiro.driver.teste;

import maucheiro.driver.pojo.GoogleDriver;
import maucheiro.driver.pojo.WebDriver;

public class TestDriverChrome implements TestDriver {

	@Override
	public String direcionarURL(String url) {
		
		WebDriver driver = new GoogleDriver();
		
		driver.setUrl(url);
		
		return driver.abrirBrowser();
	}

}
