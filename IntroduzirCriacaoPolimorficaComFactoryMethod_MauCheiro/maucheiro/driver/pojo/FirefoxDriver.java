package maucheiro.driver.pojo;

public class FirefoxDriver extends WebDriver {

	
	//...
	
	@Override
	protected String recuperarBrowser() {
		return "Mozilla Firefox";
	}

	@Override
	public String abrirBrowser() {
		return "Abrindo browser " + recuperarBrowser();
	}
}
