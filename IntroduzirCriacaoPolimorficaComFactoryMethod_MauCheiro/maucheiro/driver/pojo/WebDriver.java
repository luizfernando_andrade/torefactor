package maucheiro.driver.pojo;

public abstract class WebDriver {

	private String url;

	abstract protected String recuperarBrowser();
	
	public abstract String abrirBrowser();
	
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String direcionarURL(){
		return "Abrindo p�gina: " + getUrl();
	}
	
	
}
