package refatoracao.bhaskara.pojo;

public class BhaskaraX2 extends BhaskaraComposite {
	
	public BhaskaraX2(Double a, Double b, DeltaComposite delta) {
		super(a, b, delta);
	}

	public double calcularBhaskaraX2() {
		return (-b - raizDelta())/2*a;
	}

}
