package refatoracao.bhaskara.pojo;

public class BhaskaraComposite {

	private DeltaComposite delta;
	protected Double a;
	protected Double b;

	public BhaskaraComposite(Double a, Double b, DeltaComposite delta) {
		this.a = a;
		this.b = b;
		this.delta = delta;
	}
	
	public double raizDelta() {
		return Math.sqrt( delta.calcularDelta());
	}
}
