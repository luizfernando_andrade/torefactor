package refatoracao.bhaskara.pojo;

public class BhaskaraX1 extends BhaskaraComposite {
	

	public BhaskaraX1(Double a, Double b, DeltaComposite delta) {
		super(a, b, delta);
	}
	
	public double calcularBhaskaraX1() {
		return (-b + raizDelta())/2*a;
	}


}
