package refatoracao.bhaskara.teste;

import java.util.HashMap;

import org.junit.Test;

import junit.framework.Assert;
import maucheiro.bhaskara.pojo.Expressao;

public class TestarFuncao {

	@Test
	public void testarBhaskaraX1(){
		Expressao bhaskara = new Expressao();
		HashMap<String, Double> resultado = (HashMap<String, Double>) 
				bhaskara.calcularBhaskara(1.0, -2.0, -3.0);
		
		Double x1 = resultado.get("x1");
		
		System.out.println("X1 = " + x1 );
		
		Assert.assertTrue(x1 == 3.0);
	}
	@Test
	public void testarBhaskaraX2(){
		Expressao bhaskara = new Expressao();
		HashMap<String, Double> resultado = (HashMap<String, Double>) 
				bhaskara.calcularBhaskara(1.0, -2.0, -3.0);
		
		Double x2 = resultado.get("x2");
		
		System.out.println("X2 = " + x2 );
		
		Assert.assertTrue(x2 == -1.0);
	}
}
