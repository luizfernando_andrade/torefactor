package refatoracao.costrutor.funcionario;

import java.util.Date;
import java.util.List;

public class Funcionario {
	
	private String nome;
	private String cpf;
	private Date nascimento;
	private List<Dependente> dependentes;
	private Boolean possuiCNH;
	
	public Funcionario( String nome, String cpf, Date nascimento, 
			List<Dependente> dependentes, Boolean possuiCNH) {
		this.nome = nome;
		this.cpf = cpf;
		this.nascimento = nascimento;
		this.dependentes = dependentes;
		this.possuiCNH = possuiCNH;
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			List<Dependente> dependentes) {
		this(nome, cpf, nascimento, dependentes, null);
	}
	public Funcionario( String nome, String cpf, Date nascimento) {
		this(nome, cpf, nascimento, null, null);
	}
	public Funcionario( String nome, String cpf, Date nascimento, 
			boolean possuiCNH) {
		this(nome, cpf, nascimento, null, possuiCNH);
	}
	
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public Date getNascimento() {
		return nascimento;
	}
	public void setNascimento(Date nascimento) {
		this.nascimento = nascimento;
	}
	public List<Dependente> getDependentes() {
		return dependentes;
	}
	public void setDependentes(List<Dependente> dependentes) {
		this.dependentes = dependentes;
	}

	public Boolean isPossuiCNH() {
		return possuiCNH;
	}

	public void setPossuiCNH(Boolean possuiCNH) {
		this.possuiCNH = possuiCNH;
	}
	

}
