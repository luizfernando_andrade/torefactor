package refatoracao.contrutor.teste;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.costrutor.funcionario.Dependente;
import refatoracao.costrutor.funcionario.Funcionario;

public class TestarFuncionario {
	
	@Test
	public void testarConstrutorCompleto(){
		Calendar nascimento = new GregorianCalendar(1988, 10, 10);
		List<refatoracao.costrutor.funcionario.Dependente> dependentes = new ArrayList<>();
		dependentes.add(new Dependente("Roberta"));
		Funcionario funcionarioDadosCompletos = 
				new Funcionario("Ronaldo", "12345678909",nascimento.getTime(),
						dependentes, Boolean.TRUE);
		
		Assert.assertTrue(funcionarioDadosCompletos.getNome()!= null
						&& funcionarioDadosCompletos.getCpf() != null
						&& funcionarioDadosCompletos.getNascimento() != null
						&& funcionarioDadosCompletos.getDependentes().size() > 0
						&& funcionarioDadosCompletos.isPossuiCNH());
	}
	@Test
	public void testarConstrutorSemCNHSemDependentes(){
		Calendar nascimento = new GregorianCalendar(1988, 10, 10);
		Funcionario funcionarioDadosCompletos = 
				new Funcionario("Ronaldo", "12345678909",nascimento.getTime());
		
		Assert.assertTrue(funcionarioDadosCompletos.getNome()!= null
					   && funcionarioDadosCompletos.getCpf() != null
				       && funcionarioDadosCompletos.getNascimento() != null);
	}

}
