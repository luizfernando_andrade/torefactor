package refatoracao.garagem.teste;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import junit.framework.Assert;
import refatoracao.garagem.pojo.Carro;
import refatoracao.garagem.pojo.Garagem;

public class TestarGaragem {

	@Test
	public void testarListaEspera(){
		
		List<Carro> carros = new ArrayList<Carro>();
		carros.add(new Carro("VW", "GOL"));
		carros.add(new Carro("Audi", "A3"));
		carros.add(new Carro("VW", "Saveiro"));
		carros.add(new Carro("Audi", "TT"));
		
		Garagem garagem = new Garagem();
		
		for (Carro carro : carros) {
			garagem.validarEntrada(carro);
		}
		System.out.println(garagem.getFilaEspera());
		Assert.assertTrue(garagem.getFilaEspera() == 1);
	}
}
