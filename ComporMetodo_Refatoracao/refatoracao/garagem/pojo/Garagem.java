package refatoracao.garagem.pojo;

import java.util.ArrayList;
import java.util.List;

public class Garagem {

	private List<Carro> carrosEstacionados = new ArrayList<Carro>();;
	private List<Carro> carrosEspera = new ArrayList<Carro>();;
	private Integer numeroVagas = 3;
	private Integer vagasOcupadas = 0;
	private Integer filaEspera = 0;
	
	
	public void validarEntrada(Carro carro){
		if (possuiVagas()){
			adicionarVaga(carro);
		} else {
			adicionarEspera(carro);
		}
	}
	private void adicionarEspera(Carro carro) {
		carrosEspera.add(carro);
		filaEspera++;
	}
	private void adicionarVaga(Carro carro) {
		carrosEstacionados.add(carro);
		vagasOcupadas ++;
	}
	private boolean possuiVagas() {
		return carrosEstacionados.size() < numeroVagas;
	}


	public List<Carro> getCarros() {
		return carrosEstacionados;
	}


	public void setCarros(List<Carro> carros) {
		this.carrosEstacionados = carros;
	}


	public int getNumeroVagas() {
		return numeroVagas;
	}


	public void setNumeroVagas(int numeroVagas) {
		this.numeroVagas = numeroVagas;
	}


	public Integer getVagasOcupadas() {
		return vagasOcupadas;
	}


	public Integer getFilaEspera() {
		return filaEspera;
	}


	public void setFilaEspera(Integer filaEspera) {
		this.filaEspera = filaEspera;
	}


	public List<Carro> getCarrosEspera() {
		return carrosEspera;
	}


	public void setCarrosEspera(List<Carro> carrosEspera) {
		this.carrosEspera = carrosEspera;
	}
	
}
