package refatoracao.conexao.pojo;



public class DataBaseConnection {
	
	private static DataBaseConnection dbConnection = new DataBaseConnection();

	private DataBaseConnection() {}
	
	public static DataBaseConnection getInstancia() {
		if (dbConnection == null){
			dbConnection = new DataBaseConnection();
		}
		return dbConnection;		
	}
	
	public String conectarBase(){
		return "Usu�rio conectado no banco de dados";
	}

	public String desconectarBase() {
		
		return "desconectado da base."; 
		
	}

	public String recuperarRegistros() {
		
		return "Registros recuperados";
		
	}


}
