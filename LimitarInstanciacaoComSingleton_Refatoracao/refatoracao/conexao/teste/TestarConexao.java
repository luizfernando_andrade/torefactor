package refatoracao.conexao.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.conexao.pojo.DataBaseConnection;

public class TestarConexao {

	@Test
	public void TestarDuasInstanciasBD(){
		DataBaseConnection conexaoBD1 = DataBaseConnection.getInstancia();
		
		conexaoBD1.conectarBase();
		
		conexaoBD1.recuperarRegistros();
		
		conexaoBD1.desconectarBase();
		
		DataBaseConnection conexaoBD2 = DataBaseConnection.getInstancia();
		
		conexaoBD2.conectarBase();
		
		conexaoBD2.recuperarRegistros();
		
		conexaoBD2.desconectarBase();
		
		Assert.assertTrue(conexaoBD1.equals(conexaoBD2));
	}
}
