package refatoracao.decorator.pojo;

public class CursoExtensivo extends Curso {

	@Override
	public String infoCurso() {
		return  "Curso pr� vestibular extensivo, 12 meses de dura��o";
	}
	@Override
	public String prepararAula() {
		return "Conte�do completo para o vestibular";
	}
	@Override
	public String avaliacaoSemanal() {
		return "Conte�do acumulativo para " + tipoCurso();
	}
	@Override
	public String tipoCurso() {
		return "Curso Extensivo";
	}

}
