package refatoracao.decorator.pojo;

public class AulaExtraDecorator 
			extends CursoIntensivo{

	private String diaAulaExtra;
	private CursoIntensivo curso;

	public AulaExtraDecorator(String diaAulaExtra, CursoIntensivo Curso){
		this.diaAulaExtra = diaAulaExtra;
		curso = Curso;
	}
	//m�todos adicionais
	public String infoBasica(){
		return tipoCurso() + " aula extra " + getDiaAulaExtra();
	}
	public String getDiaAulaExtra() {
		return diaAulaExtra;
	}
	public void setDiaAulaExtra(String diaAulaExtra) {
		this.diaAulaExtra = diaAulaExtra;
	}
	

	public String getDiaAulaExtra2() {
		return diaAulaExtra;
	}

	public void setDiaAulaExtra2(String diaAulaExtra2) {
		this.diaAulaExtra = diaAulaExtra2;
	}

	public CursoIntensivo getCurso() {
		return curso;
	}

	public void setCurso(CursoIntensivo curso) {
		this.curso = curso;
	}
	
}
