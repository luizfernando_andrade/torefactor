package refatoracao.decorator.pojo;

public class CursoIntensivo extends Curso{


	@Override
	public String infoCurso() {
		return "Curso pr� vestibular intensivo, 6 meses de dura��o";
	}

	@Override
	public String prepararAula() {
		return "Conte�do sintetizado para o vestibular";
	}

	@Override
	public String avaliacaoSemanal() {
		return "Conte�do acumulativopara " + tipoCurso();
	}

	@Override
	public String tipoCurso() {
		return "Curso Intensivo";
	}

}
