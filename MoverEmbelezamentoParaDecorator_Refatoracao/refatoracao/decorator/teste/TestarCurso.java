package refatoracao.decorator.teste;

import org.junit.Assert;
import org.junit.Test;

import refatoracao.decorator.pojo.AulaExtraDecorator;
import refatoracao.decorator.pojo.CursoIntensivo;

public class TestarCurso {

	@Test
	public void testarCursoIntensivo(){
		String aulaExtra = "sabado";
		
		CursoIntensivo cursoIntensivo = new CursoIntensivo();
		
		AulaExtraDecorator cursoDecorado = new AulaExtraDecorator(aulaExtra, cursoIntensivo);
		
		String informacaoCurso = cursoDecorado.infoBasica();
		
		Assert.assertTrue(informacaoCurso.contains("extra"));
	}
}
