package maucheiro.validacao.teste;

import org.junit.Test;

import junit.framework.Assert;
import maucheiro.validacao.pojo.ArquivoDeIntergracao;
import maucheiro.validacao.pojo.ControlePostoGasolina;
import maucheiro.validacao.pojo.TipoArquivo;

public class TestarTransmissaoDeArquivo {

	@Test
	public void testarTransmissaoFinanceiro(){
		ArquivoDeIntergracao arquivo = new ArquivoDeIntergracao();
		arquivo.setImpostosPagos(35.0);
		arquivo.setResponsavel("Gerente Financeiro");
		arquivo.setTipoArquivo(TipoArquivo.Financeiro);
		arquivo.setImpostosPagos(200.0);
		
		ControlePostoGasolina posto = new ControlePostoGasolina(arquivo);
		String validadoPor = posto.validarArquivo();
		System.out.println(validadoPor);
		
		Assert.assertTrue(validadoPor.contains("Financeiro"));
		
	}
}
