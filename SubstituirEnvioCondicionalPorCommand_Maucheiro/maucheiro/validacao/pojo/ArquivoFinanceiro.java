package maucheiro.validacao.pojo;

public class ArquivoFinanceiro {

	Double valorTotalFinanceiro;

	public Double getValorTotalFinanceiro() {
		return valorTotalFinanceiro;
	}

	public void adicionarValorTotalFinanceiro(Double valor) {
		this.valorTotalFinanceiro = valor;
	}
	
	public String setorResponsavel(){
		return "Financeiro";
	}

}
