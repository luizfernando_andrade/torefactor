package maucheiro.validacao.pojo;

public class ControlePostoGasolina {

	ArquivoDeIntergracao arquivo;
	
	public ControlePostoGasolina(ArquivoDeIntergracao arquivo) {
		this.arquivo = arquivo;
	}
	
	public String validarArquivo(){
		
		if (TipoArquivo.Financeiro.equals(arquivo.getTipoArquivo())){
			ArquivoFinanceiro financeiro = new ArquivoFinanceiro();
			financeiro.adicionarValorTotalFinanceiro(arquivo.getImpostosPagos());
			arquivo.setResponsavel(financeiro.setorResponsavel());
			return arquivo.getResponsavel();
		}
		if (TipoArquivo.Comercial.equals(arquivo.getTipoArquivo())){
			ArquivoComercial comercial = new ArquivoComercial();
			comercial.adicionarValorTotalComercial(arquivo.getImpostosPagos());
			arquivo.setResponsavel(comercial.setorResponsavel());
			return arquivo.getResponsavel();
		} else {
			ArquivoEstoque estoque= new ArquivoEstoque();
			estoque.adicionarValor(arquivo.getImpostosPagos());
			arquivo.setResponsavel(estoque.setorResponsavel());
			return arquivo.getResponsavel();
		}
	}
}
