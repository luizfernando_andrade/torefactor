package refatoracao.secretaria.pojo;

public interface AlunoEstado {

	AlunoEstado aprovado();
	
	AlunoEstado reprovado();
	
	AlunoEstado provaFinal();
	
	EstadoAluno acaoAluno();
}
