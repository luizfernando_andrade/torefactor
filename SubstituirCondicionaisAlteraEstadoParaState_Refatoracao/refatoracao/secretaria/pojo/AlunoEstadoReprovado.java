package refatoracao.secretaria.pojo;

public class AlunoEstadoReprovado implements AlunoEstado {

	@Override
	public AlunoEstado aprovado() {
		return null;
	}

	@Override
	public AlunoEstado reprovado() {
		return null;
	}

	@Override
	public AlunoEstado provaFinal() {
		return null;
	}

	@Override
	public EstadoAluno acaoAluno() {
		return EstadoAluno.Reprovado;
	}

}
