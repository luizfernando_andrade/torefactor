package refatoracao.secretaria.pojo;

public enum EstadoAluno {
	Aprovado, Reprovado, ProvaFinal;
}
