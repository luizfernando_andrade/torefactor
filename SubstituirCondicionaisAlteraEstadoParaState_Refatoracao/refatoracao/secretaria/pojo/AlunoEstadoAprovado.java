package refatoracao.secretaria.pojo;

public class AlunoEstadoAprovado implements AlunoEstado {

	@Override
	public AlunoEstado aprovado() {
		return this;
	}

	@Override
	public AlunoEstado reprovado() {
		return new AlunoEstadoReprovado();
	}

	@Override
	public AlunoEstado provaFinal() {
		return new AlunoEstadoProvaFinal();
	}

	@Override
	public EstadoAluno acaoAluno() {
		return EstadoAluno.Aprovado;
	}

}
