package refatoracao.secretaria.pojo;

public class AlunoEstadoProvaFinal implements AlunoEstado {

	@Override
	public AlunoEstado aprovado() {
		return new AlunoEstadoAprovado();
	}

	@Override
	public AlunoEstado reprovado() {
		return new AlunoEstadoReprovado();
	}

	@Override
	public AlunoEstado provaFinal() {
		return this;
	}

	@Override
	public EstadoAluno acaoAluno() {
		return EstadoAluno.ProvaFinal;
	}

}
